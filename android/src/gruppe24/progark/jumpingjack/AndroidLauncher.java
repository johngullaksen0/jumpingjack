package gruppe24.progark.jumpingjack;

import android.app.Activity;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.systems.MultiplayerSystem;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Check for Google services.
		if (isGooglePlayServicesAvailable(this)) {
			System.out.println("AVAILABLE!!");
		}
		else {
			System.out.println("UNAVAILABLE!!");
			GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
			googleApiAvailability.makeGooglePlayServicesAvailable(this);
		}

		FirebaseServerConnection connection = new FirebaseServerConnection();
		MultiplayerSystem.connection = connection;
		connection.test();

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new MyGdxGame(), config);
	}

	public boolean isGooglePlayServicesAvailable(Activity activity) {
		GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
		int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
		if(status != ConnectionResult.SUCCESS) {
			if(googleApiAvailability.isUserResolvableError(status)) {
				googleApiAvailability.getErrorDialog(activity, status, 2404).show();
			}
			return false;
		}
		return true;
	}
}
