package gruppe24.progark.jumpingjack;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gruppe24.progark.jumpingjack.util.network.HighscoreCallback;
import gruppe24.progark.jumpingjack.util.network.MultiPlayerData;
import gruppe24.progark.jumpingjack.util.network.NetworkCallback;
import gruppe24.progark.jumpingjack.util.network.ServerConnection;

/*
    An implementation of the 'ServerConnection' class for the android platform. Uses firebase real time database.
*/
public class FirebaseServerConnection extends ServerConnection {

    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference room;

    String highScoreLocation;

    ValueEventListener updatedPlayerListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            //System.out.println(dataSnapshot.getValue());
            ArrayList<MultiPlayerData> mpdl = new ArrayList<>();
            for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                MultiPlayerData mpd = postSnapshot.getValue(MultiPlayerData.class);
                mpdl.add(mpd);
            }

            if (callback != null) callback.networkCallback(mpdl);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            // on failure
        }
    };
    ValueEventListener updatedHighscoreListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            HashMap<String, Integer> scoreList = new HashMap<>();
            for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                Integer score = postSnapshot.getValue(Integer.class);
                scoreList.put(postSnapshot.getKey(), score);
            }

            if (highscoreCallback != null) highscoreCallback.highscoreCallback(scoreList);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            // on failure
        }
    };

    public FirebaseServerConnection() {
        super();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("rooms");
    }

    public void test() {
        DatabaseReference dbref = database.getReference("message");
        dbref.setValue("THIS IS A MESSAGE");
        System.out.println("done test");
    }

    @Override
    public void connectToRoom(String roomName) {
        super.connectToRoom(roomName);
        room = myRef.child(roomName);
        room.child("players").addValueEventListener(updatedPlayerListener);
        room.child(highScoreLocation).addValueEventListener(updatedHighscoreListener);
    }

    @Override
    public void closeRoom() {
        super.closeRoom();
        room.child("players").removeEventListener(updatedPlayerListener);
        room.child(highScoreLocation).removeEventListener(updatedHighscoreListener);
        callback = null;
        highscoreCallback = null;
    }

    @Override
    public void registerCallback(NetworkCallback callback) {
        super.registerCallback(callback);
    }

    @Override
    public void updatePlayerData(MultiPlayerData data) {
        super.updatePlayerData(data);
        room.child("players").child(data.name).setValue(data);
    }

    @Override
    public void registerHighscoreCallback(final HighscoreCallback callback, boolean onlyOnce) {
        if (onlyOnce) {
            room.child(highScoreLocation).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    HashMap<String, Integer> scoreList = new HashMap<>();
                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                        Integer score = postSnapshot.getValue(Integer.class);
                        scoreList.put(postSnapshot.getKey(), score);
                    }

                    callback.highscoreCallback(scoreList);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else {
            super.registerHighscoreCallback(callback, onlyOnce);
        }
    }

    @Override
    public void updateHighScore(String playerName, int score) {
        super.updateHighScore(playerName, score);
        room.child(highScoreLocation).child(playerName).setValue(score);
    }

    @Override
    public void setHighscoreLocation(String location) {
        super.setHighscoreLocation(location);

        this.highScoreLocation = location;
    }
}
