package gruppe24.progark.jumpingjack;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/*
    Singleton meant to keep settings and data consistent across the whole application.
 */
public class JumpingJack {

    public static JumpingJack instance = new JumpingJack();

    public AssetManager assetManager;
    public EventManager eventManager;
    public SpriteBatch spriteBatch;

    public int gameWidth = 480;
    public int gameHeight = 720;

    private JumpingJack() {
        assetManager = new AssetManager();
        eventManager = new EventManager();
    }

    public void setup(SpriteBatch batch) {
        spriteBatch = batch;
        assetManager.loadImages();
        assetManager.loadSounds();
    }
}
