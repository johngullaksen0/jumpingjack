package gruppe24.progark.jumpingjack;

import com.badlogic.ashley.core.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
    Class for managing events.
    This can be events for collisions, deaths and more.
    Uses observer pattern. Observers registers for events with subscribe method, giving the entity observed, the type of Event, and the callback.
    Subjects notify of events happening through the notify method, giving the type of event, the entity it's happening to, and eventually another entity involved in the event.
 */
public class EventManager {

    HashMap<Entity, HashMap<Integer, List<EntityEventCallback>>> entityEvents;

    public EventManager() {
        entityEvents = new HashMap<>();
    }

    /*
    Notifies any subscribers of an event of type 'eventId' happening to the involved entities. e1 is the main subject of the event.
     */
    public void notify(int eventId, Entity e1, Entity e2) {
        if (entityEvents.containsKey(e1) && entityEvents.get(e1).containsKey(eventId)) {
            List<EntityEventCallback> list = entityEvents.get(e1).get(eventId);
            for (EntityEventCallback callback : list) {
                callback.entityEventCallback(e1, e2);
            }
        }
    }

    /*
    Subscribes to any event of the type eventId, involving the entity 'entity'. 'callback' is called when such an event is happening.
     */
    public EntityEventCallback subscribe(Entity entity, int eventId, EntityEventCallback callback) {
        HashMap<Integer, List<EntityEventCallback>> eventList;
        if (!entityEvents.containsKey(entity)) {
            eventList = new HashMap<>();
            entityEvents.put(entity, eventList);
        }
        else {
            eventList = entityEvents.get(entity);
        }
        if (!eventList.containsKey(eventId)) {
            eventList.put(eventId, new ArrayList<EntityEventCallback>());
        }
        eventList.get(eventId).add(callback);
        return callback;
    }
    /*
    Unsubscribes to any event of the type eventId, involving the entity 'entity'. 'callback' is called when such an event is happening.
     */
    public void unsubscribe(Entity entity, int eventId, EntityEventCallback callback) {
        if (entityEvents.containsKey(entity)) {
            HashMap<Integer, List<EntityEventCallback>> eventList = entityEvents.get(entity);
            if (eventList.containsKey(eventId)) {
                eventList.get(eventId).remove(callback);
            }
        }
    }
    /*
    Clears all events and callbacks. Used for disposing.
     */
    public void clear() {
        entityEvents.clear();
    }
}
