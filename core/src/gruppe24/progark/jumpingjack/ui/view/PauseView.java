package gruppe24.progark.jumpingjack.ui.view;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import gruppe24.progark.jumpingjack.ui.presenter.GamePresenter;
import gruppe24.progark.jumpingjack.ui.presenter.PausePresenter;


public class PauseView extends BaseView {

    GamePresenter presenter;
    PausePresenter pausePresenter;


    //Creating different buttons that can be displayed with desired font
    private TextButton resume;
    private Label pause;
    private Label level;
    private String string;

    public PauseView(final GamePresenter presenter, final Game game, Boolean isHard) {
        super(game);
        this.presenter = presenter;
        pausePresenter = new PausePresenter(game, this);


        //Defines text for button
        resume = new TextButton("RESUME", skin);
        music = new Label("MUSIC ON", skin, "default");
        menuBtn = new TextButton("MENU", skin);
        pause = new Label("PAUSE", skin, "title");
        if(isHard){
            string = "Difficulty: Hard";
        }
        else{
            string = "Difficulty: Easy";
        }

        level = new Label(string, skin, "default");


        //SpriteBatch batch = new SpriteBatch();
        stage = new Stage(gamePort, presenter.batch);
        Gdx.input.setInputProcessor(stage);


        //When play is clicked --> taken to the game screen
        resume.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                presenter.setGameState(GamePresenter.State.RESUME);
                presenter.handleInput();
                dispose();
            }
        });

        menuBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                presenter.changeScreen(presenter.state.MAINMENU);
            }
        });

        checkbox = new CheckBox("", skin);
        checkbox.setChecked(presenter.isMusicEnabled());
        checkbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                presenter.handleMusic(checkbox.isChecked());
            }
        });

    }

    @Override
    public void show() {
        //font.setColor(Color.BLACK);
        //font.scale(5);

        //Lager en tabell for å kunne lage organisere ting i stagen vår
        Table table = new Table();
        //setter den til toppen av stagen vår
        table.top();
        // tabellen er nå samme størrelse som stagen
        table.setFillParent(true);

        //Nødløsning for å lage litt rom på toppen av tabellen
        table.pad(75);
        //adding every button to table using row to
        table.add(pause).center();
        table.row().pad(20,0,0,10);
        table.add(level);
        table.row().pad(20,0,0,10);
        table.add(music);
        table.add(checkbox).left();
        table.row().pad(20,0,0,10);
        table.add(resume).expandX().center();
        table.row().pad(20,0,0,10);
        table.add(menuBtn).center();

        //Adding table to stage
        stage.addActor(table);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
