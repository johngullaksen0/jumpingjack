package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


import gruppe24.progark.jumpingjack.model.MyPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import gruppe24.progark.jumpingjack.ui.presenter.SettingsPresenter;

public class SettingsView extends BaseView {

    SettingsPresenter settingsPresenter;

    CheckBox hardBox;
    CheckBox easyBox;
    Label easy;
    Label hard;
    TextButton resetTutorialBtn;
    Label resetText;
    Label theme;

    public HashMap<Integer, Texture> backgrounds = new HashMap<Integer, Texture>();
    private ArrayList<String> themes = new ArrayList<>();
    private static int currentBackground = 1;



    public SettingsView(final Game game) {
        super(game);
        settingsPresenter = new SettingsPresenter(game, this);


        menuBtn = new TextButton("MENU", skin);
        title = new Label("SETTINGS", skin, "title");
        music = new Label("MUSIC ON", skin, "default");
        difficulty = new Label("DIFFICULTY", skin, "default");

        resetTutorialBtn = new TextButton("Reset Tutorial", skin);
        resetText = new Label("", skin, "default");
        
        prevBtn = new TextButton("<<", skin);
        nextBtn = new TextButton(">>", skin);
        theme = new Label(themes.get(currentBackground-1), skin, "default");

        easy = new Label("EASY", skin, "subtitle");
        hard = new Label("HARD", skin, "subtitle");
        hardBox = new CheckBox("", skin);
        easyBox = new CheckBox("", skin);

        hardBox.setChecked(settingsPresenter.isHardMode());
        //easyBox.setChecked(!settingsPresenter.isHardMode());
        easyBox.setChecked(!hardBox.isChecked());

        //loops through game player characters
      /*  for(int i = 1; i <= backgrounds.size(); i++){
            if(i == currentBackground){
                background = backgrounds.get(i);
                break;
            }
        }*/



        stage = new Stage(gamePort, settingsPresenter.batch);
        Gdx.input.setInputProcessor(stage);

        menuBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                settingsPresenter.handleInput(SettingsPresenter.State.MAINMENU);
            }
        });

        checkbox = new CheckBox("", skin);
        checkbox.setChecked(settingsPresenter.isMusicEnabled());
        checkbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                settingsPresenter.handleMusic(checkbox.isChecked());

            }
        });

        easyBox.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(!easyBox.isChecked()){
                    easyBox.setChecked(true);
                }
                else{
                    hardBox.setChecked(false);
                    settingsPresenter.setDificulity(hardBox.isChecked());

                }
            }
        });


        hardBox.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(!hardBox.isChecked()){
                    hardBox.setChecked(true);
                }
                else{
                    easyBox.setChecked(false);
                    settingsPresenter.setDificulity(hardBox.isChecked());
                }
            }
            });
        resetTutorialBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                settingsPresenter.resetTutorial();
                resetText.setText("Tutorial reset");
            }
        });

        //when right arrow is clicked, next game character appears
        nextBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(currentBackground == backgrounds.size()) {
                    currentBackground= 1;
                }
                else{
                    currentBackground += 1;
                }
                changeBackground();
                theme.setText(themes.get(currentBackground-1));
            }

        });

        //when left arrow is clicked, previous game character appears
        prevBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(currentBackground == 1){
                    currentBackground = backgrounds.size();
                }
                else{
                    currentBackground -= 1;
                }
                changeBackground();
                theme.setText(themes.get(currentBackground-1));
            }
        });

    }

    public static void setCurrentBackground(int background){
        currentBackground = background;
    }

    public void addBackgrounds(int i, Texture texture){
        backgrounds.put(i, texture);
    }

    public void addThemes(String theme){
        themes.add(theme);
    }


    public void changeBackground(){
        for(int i = 1; i <= backgrounds.size(); i++){
            if(i == currentBackground){
                background = backgrounds.get(i);
                settingsPresenter.setBackground(currentBackground, background);
                break;
            }
        }
    }


    @Override
    public void show() {

        //Lager en tabell for å kunne lage organisere ting i stagen vår
        Table table = new Table();
        //table.setDebug(true); // turn on all debug lines (table, cell, and widget)
        //setter den til toppen av stagen vår
        table.top();
        // tabellen er nå samme størrelse som stagen
        table.setFillParent(true);
        table.pad(75);
        table.add(title).colspan(3);
        table.row().pad(30,0,0,10);
        table.add(prevBtn);
        table.add(theme);
        table.add(nextBtn);
        table.row().pad(20,0,0,10);
        table.add(music).colspan(3);
        table.add(checkbox).left();
        table.row().pad(20,0,0,10);
        table.add(difficulty).colspan(3);
        table.row().pad(10,0,0,10);
        table.add(easy).colspan(3);
        table.add(easyBox).colspan(3);
        table.row().pad(10,0,0,10);
        table.add(hard).colspan(3);
        table.add(hardBox).colspan(3);
        table.row().pad(20,0,0,10);
        table.add(menuBtn).colspan(3);
        table.row().pad(20,0,0,10);
        table.add(resetTutorialBtn).colspan(3);
        table.row().pad(0,0,0,10);
        table.add(resetText).colspan(3);



        //Adding table to stage
        stage.addActor(table);

    }

    @Override
    public void render(float delta) {
        super.render(delta);

    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    //to avoid memory leaks
    @Override
    public void dispose() {
        stage.dispose();

    }
}

