package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;

import gruppe24.progark.jumpingjack.ui.presenter.MultiplayerPresenter;

public class MultiplayerView extends BaseView {

    MultiplayerPresenter presenter;

    public MultiplayerView(Game game) {
        super(game);
        presenter = new MultiplayerPresenter(game, this);
    }

    @Override
    public void show() {
        presenter.connectToRoom();
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
