package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import gruppe24.progark.jumpingjack.ui.presenter.GameOverPresenter;
import gruppe24.progark.jumpingjack.ui.presenter.HighScorePresenter;

public class GameOverView extends BaseView{

    GameOverPresenter gameOverPresenter;

    private Label gameOver;
    private Label scoreLabel;
    private Label highscoreLabel;
    private int score;
    private int highscore;

    public GameOverView(Game game, int score, int highscore) {
        super(game);
        gameOverPresenter = new GameOverPresenter(game, this);

        this.score = score;
        this.highscore = highscore;

        gameOver = new Label("GAME OVER",skin, "title");
        scoreLabel = new Label(String.format("Your score: %06d", score), skin, "default");
        highscoreLabel = new Label(String.format("Your High Score: %06d ", highscore), skin, "default" );

        playBtn = new TextButton("PLAY AGAIN", skin);
        menuBtn = new TextButton("MENU",skin);

        //stage er som en tom boks
        stage = new Stage(gamePort, gameOverPresenter.batch);

        //gjør det mulig for stagen å ta inn input fra brukeren
        Gdx.input.setInputProcessor(stage);

        //funksjoner for å håndtere input når brukeren trykker på knappene
        playBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                gameOverPresenter.handleInput(HighScorePresenter.State.GAME);
            }
        });
        menuBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                gameOverPresenter.handleInput(HighScorePresenter.State.MAINMENU);
            }
        });

    }

    @Override
    public void show() {
        //Lager en tabell for å kunne lage organisere ting i stagen vår
        Table table = new Table();
        //setter den til toppen av stagen vår
        table.top();
        // tabellen er nå samme størrelse som stagen
        table.setFillParent(true);
        table.pad(75);
        //expandX(), gjør at labelen er lengden av skjermen
        table.add(gameOver).expandX();
        //row gjør at det blir en ny rad i tabellen, alt under kommer på neste rad
        table.row().pad(20,0,0,10);
        //legger til alle scoresene i tabellen på hver sin rad
        table.add(scoreLabel);
        table.row().pad(20,0,0,10);
        table.add(highscoreLabel);
        table.row().pad(20,0,0,10);
        table.add(playBtn).expandX();
        table.row().pad(20,0,0,10);
        table.add(menuBtn).expandX();

        //legger til tabbelen i stagen
        stage.addActor(table);
    }
}
