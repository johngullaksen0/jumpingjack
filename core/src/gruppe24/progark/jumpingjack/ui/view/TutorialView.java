package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.ui.presenter.TutorialPresenter;

public class TutorialView extends BaseView {

    TutorialPresenter presenter;
    int _stage = 1; // How far the tutorial has gone.

    public TutorialView(Game game) {
        super(game);
        presenter = new TutorialPresenter(game, this);
        background = JumpingJack.instance.assetManager.getImage("tutorial1");

        stage = new Stage(gamePort, JumpingJack.instance.spriteBatch);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if (Gdx.input.justTouched()) {
            _stage++;
            if (_stage > 3) {
                presenter.handleInput();
            }
            else {
                background = JumpingJack.instance.assetManager.getImage("tutorial" + _stage);
            }
        }
    }
}
