package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import java.util.HashMap;
import gruppe24.progark.jumpingjack.ui.presenter.MainMenuPresenter;


public class MainMenuView extends BaseView {

    MainMenuPresenter mainMenuPresenter;


    public HashMap<Integer, SpriteDrawable> playerSprite = new HashMap<Integer, SpriteDrawable>();

    public Image player;
    private static int currentCharacter = 1;


    public MainMenuView(final Game game) {
        super(game);
        mainMenuPresenter = new MainMenuPresenter(game, this);
        stage = new Stage(gamePort, mainMenuPresenter.batch);
        Gdx.input.setInputProcessor(stage);


        playBtn = new TextButton("PLAY", skin);
        multiplayerButton = new TextButton("MULTIPLAYER", skin);
        highscore  = new TextButton("HIGH SCORE", skin);
        settings = new TextButton("SETTINGS", skin);
        title = new Label("JUMPING JACK", skin, "title");
        quitBtn = new TextButton("QUIT", skin);
        prevBtn = new TextButton("<<", skin);
        nextBtn = new TextButton(">>", skin);



        //loops through game player characters
        for(int i = 1; i <= playerSprite.size(); i++){
            if(i == currentCharacter){
                player = new Image(playerSprite.get(i));
                break;
            }
        }


        //when right arrow is clicked, next game character appears
        nextBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(currentCharacter == playerSprite.size()) {
                    currentCharacter = 1;
                }
                else{
                    currentCharacter += 1;
                }
                changePlayer();
            }

        });

        //when left arrow is clicked, previous game character appears
        prevBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(currentCharacter == 1){
                    currentCharacter = playerSprite.size();
                    }
                  else{
                    currentCharacter -= 1;
                  }
                changePlayer();
            }
        });




        //When play is clicked --> taken to the game screen
        playBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                mainMenuPresenter.setSinglePlayer();
                mainMenuPresenter.handleInput(MainMenuPresenter.State.GAME);
            }
        });
        //When play is clicked --> taken to the game screen
        multiplayerButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                mainMenuPresenter.handleInput(MainMenuPresenter.State.MULTIPLAYER);

            }
        });
        settings.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                mainMenuPresenter.handleInput(MainMenuPresenter.State.SETTINGS);
            }
        });

        highscore.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                mainMenuPresenter.handleInput(MainMenuPresenter.State.HIGHSCORE);
            }
        });

        quitBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
                dispose();
            }
        });

    }

    public static void setCurrentCharacter(int player){
        currentCharacter = player;
    }

    public void addCharacters(int i, Texture texture){
        Sprite sprite = new Sprite(texture);
        SpriteDrawable drawable = new SpriteDrawable(sprite);
        playerSprite.put(i, drawable);
    }


    public void changePlayer(){
        for(int i = 1; i <= playerSprite.size(); i++){
            if(i == currentCharacter){
                player.setDrawable(playerSprite.get(i));
                mainMenuPresenter.changeCharacter(currentCharacter);
                break;
            }
        }
    }




    @Override
    public void show() {

        //Lager en tabell for å kunne lage organisere ting i stagen vår
        Table table = new Table();
        //setter den til toppen av stagen vår
        table.top();
        // tabellen er nå samme størrelse som stagen
        table.setFillParent(true);
        table.pad(75);
        table.add(title).colspan(3);
        table.row().pad(30, 0, 10, 0);
        table.add(prevBtn).right();
        table.add(player);
        table.add(nextBtn).left();
        table.row().padBottom(10);
        //adding every button to table using row to
        table.add(playBtn).colspan(3);
        table.row().padBottom(10);
        table.add(multiplayerButton).colspan(3);
        table.row().padBottom(10);
        table.add(highscore).colspan(3);
        table.row().padBottom(10);
        table.add(settings).colspan(3);
        table.row().padBottom(10);
        table.add(quitBtn).colspan(3);

        //Adding table to stage
        stage.addActor(table);
        //table.setDebug(true); // turn on all debug lines (table, cell, and widget)

    }

    @Override
    public void render(float delta) {
        super.render(delta);

    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }


    //to avoid memory leaks
    @Override
    public void dispose() {
        stage.dispose();
    }
}




