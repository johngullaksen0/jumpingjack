package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import gruppe24.progark.jumpingjack.ui.presenter.GamePresenter;

/*
    View of the UI Game.
 */
public class GameView extends BaseView {

    GamePresenter gamePresenter;




    private int score = 0;

    private static Label scoreLabel;
    public static Label highScoreLabel;

    Label yourScore;
    TextButton pause;


    public GameView(Game game){
        super(game);
        gamePresenter = new GamePresenter(this.game, this);


        //sets the stage with the spritebatch and viewport
        stage = new Stage(gamePort, gamePresenter.batch);


        //Adds a table for score, and quit and pause button.
        Table table = new Table();
        table.top();
        table.setFillParent(true);


        yourScore = new Label("Score", skin, "default");
        pause = new TextButton("Pause Game", skin);
        scoreLabel = new Label(String.format("%06d", score), skin, "subtitle");
        highScoreLabel = new Label((""), skin, "subtitle");



        table.pad(10, 20, 0, 20);
        table.add(yourScore).expandX().left();
        table.add(pause).expandX().right();
        table.row().pad(5,0,0,10);
        table.add(scoreLabel).left();
        table.add(highScoreLabel).right();

        //add the table to the stage
        stage.addActor(table);


    }

    public static void setScoreLabel(String score){
        scoreLabel.setText(score);
    }

    @Override
    public void show() {
        //set the stage to listen to input from users
        Gdx.input.setInputProcessor(stage);
        pause.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                gamePresenter.handleInput();
            }
        });

    }

    @Override
    public void render(float delta) {
        gamePresenter.update(delta);
        gamePresenter.batch.setProjectionMatrix(stage.getCamera().combined);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        gamePresenter.resize(width, height);
        gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        gamePresenter.batch.dispose();
        stage.dispose();
    }
}
