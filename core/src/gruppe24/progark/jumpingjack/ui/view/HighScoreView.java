package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import gruppe24.progark.jumpingjack.model.MyPreferences;
import gruppe24.progark.jumpingjack.ui.presenter.HighScorePresenter;

public class HighScoreView extends BaseView {

    HighScorePresenter highScorePresenter;

    private int easyHighScore;
    private int hardHighScore;
    public Label easyHighScoreLabel;
    private Label hardHighScoreLabel;
    private Label easyLabel;
    private Label hardLabel;
    private MyPreferences prefs;


    public HighScoreView(Game game) {
        //tar inn game og starter opp en ny highscore presenter for å lage en referanse til hverandre
        super(game);
        highScorePresenter = new HighScorePresenter(game, this);


        //Defines text for button
        menuBtn = new TextButton("MENU", skin);
        title = new Label("HIGH SCORES",skin, "title");
        easyLabel = new Label("EASY",skin, "default");
        hardLabel = new Label("HARD",skin, "default");


        easyHighScoreLabel = new Label(Integer.toString(highScorePresenter.getHighScore("easy")), skin, "subtitle");
        hardHighScoreLabel = new Label(Integer.toString(highScorePresenter.getHighScore("hard")), skin, "subtitle");


        //stage er som en tom boks
        stage = new Stage(gamePort, highScorePresenter.batch);

        //gjør det mulig for stagen å ta inn input fra brukeren
        Gdx.input.setInputProcessor(stage);

        //funksjoner for å håndtere input når brukeren trykker på knappene
        menuBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                highScorePresenter.handleInput(HighScorePresenter.State.MAINMENU);
            }
        });
    }

    /*public void setGlobalHighScore(int globalHighScore) {
        this.globalHighScore = globalHighScore;
    }*/


    @Override
    public void show() {
        //Lager en tabell for å kunne lage organisere ting i stagen vår
        Table table = new Table();
        //setter den til toppen av stagen vår
        table.top();
        // tabellen er nå samme størrelse som stagen
        table.setFillParent(true);
        table.pad(75);
        //expandX(), gjør at labelen er lengden av skjermen
        table.add(title).expandX();
        //row gjør at det blir en ny rad i tabellen, alt under kommer på neste rad
        table.row().pad(20,0,0,10);
        table.add(easyLabel);
        table.row().pad(20,0,0,10);
        //legger til alle scoresene i tabellen på hver sin rad
        table.add(easyHighScoreLabel);
        table.row().pad(20,0,0,10);
        table.add(hardLabel);
        table.row().pad(20,0,0,10);
        table.add(hardHighScoreLabel);
        table.row().pad(40, 0,0,10);
        table.add(playBtn).expandX();
        table.row().pad(20,0,0,10);
        table.add(menuBtn).expandX();

        //legger til tabbelen i stagen
        stage.addActor(table);
    }


    @Override
    public void dispose() {
        stage.dispose();
    }
}
