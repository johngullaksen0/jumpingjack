package gruppe24.progark.jumpingjack.ui.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;

public abstract class BaseView implements Screen {

    public Game game;
    public MyGdxGame myGame;

    protected Skin skin;
    //Creating different buttons that can be displayed with desired font
    protected TextButton playBtn;
    protected TextButton multiplayerButton;
    protected TextButton highscore;
    protected TextButton settings;
    protected TextButton quitBtn;
    protected TextButton menuBtn;


    protected Texture background;
    protected Label title;
    protected Label music;
    protected Label difficulty;
    protected CheckBox checkbox;
    protected TextButton prevBtn;
    protected TextButton nextBtn;



    Stage stage;
    protected OrthographicCamera gamecam;
    protected Viewport gamePort;

    public BaseView(Game game){
        this.game =game;
        background = new Texture("gym.png");
        skin = new Skin(Gdx.files.internal("skin/skinui.json"));


        //gamecan is what follows along out gameworld and what the viewport actually displays
        gamecam = new OrthographicCamera();
        //Viewport is basically a window into our game and how our game look on each individual devices,
        //this is because different devices have different size on the screen
        //strechViewport trekker texturene in og ut, men beholder forholdet mellom dem, kan også eventuelt bruke fitViewport
        //verdiene jeg har tatt inn for width og height er bare noe jeg har prøvd meg frem til og funket best med texturene vi har
        gamePort = new StretchViewport(JumpingJack.instance.gameWidth, JumpingJack.instance.gameHeight, gamecam);
    }
    public void setBackground(Texture background) {
        this.background = background;
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1,1,1, 1 );
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //tegner stagen til skjermen
        stage.getBatch().begin();
        stage.getBatch().draw(background, 0, 0, JumpingJack.instance.gameWidth, JumpingJack.instance.gameHeight);
        stage.getBatch().end();
        stage.draw();

    }


    //oppdaterer størrelsen på viewporten når størrelsen på skjermen forandres
    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }
}
