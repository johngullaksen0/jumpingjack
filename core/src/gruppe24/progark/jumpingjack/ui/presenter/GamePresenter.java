package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.EntityBuilder;
import gruppe24.progark.jumpingjack.ecs.components.MultiplayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.MultiplayerScoreComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.systems.CoinSystem;
import gruppe24.progark.jumpingjack.ecs.systems.CollisionSystem;
import gruppe24.progark.jumpingjack.ecs.systems.MultiplayerSystem;
import gruppe24.progark.jumpingjack.ecs.systems.PlatformSystem;
import gruppe24.progark.jumpingjack.ecs.systems.PlayerSystem;
import gruppe24.progark.jumpingjack.ecs.systems.PowerupSystem;
import gruppe24.progark.jumpingjack.ecs.systems.RenderSystem;
import gruppe24.progark.jumpingjack.ecs.systems.VelocitySystem;
import gruppe24.progark.jumpingjack.model.MyPreferences;
import gruppe24.progark.jumpingjack.model.PlayerData;
import gruppe24.progark.jumpingjack.ui.view.GameOverView;
import gruppe24.progark.jumpingjack.ui.view.GameView;
import gruppe24.progark.jumpingjack.ui.view.PauseView;


/*
    Presenter of  the Game.
*/
public class GamePresenter extends BasePresenter {

    private RenderSystem renderSystem;
    private int currentPlayerCount;

    // The engine of the ecs system. The model of the MVP pattern.
    PooledEngine engine;

    private ComponentMapper<PlayerComponent> plm = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<MultiplayerComponent> mplm = ComponentMapper.getFor(MultiplayerComponent.class);
    private ComponentMapper<MultiplayerScoreComponent> mscorm = ComponentMapper.getFor(MultiplayerScoreComponent.class);
    private ImmutableArray<Entity> entities;

    //sets state for if the game is running or paused, default run
    public State state = State.RUN;

    private GameView view;

    public GamePresenter(Game game, GameView view) {
        super(game, view);

        this.view = view;
        Random rand = new Random();
        engine = new PooledEngine();


        currentPlayerCount = PlayerData.getCurrentPlayer();

        //Adding systems. Priority from lowest to highest.
        renderSystem = new RenderSystem(10);
        engine.addSystem(renderSystem);
        engine.addSystem(new VelocitySystem(1));
        engine.addSystem(new PlayerSystem(1));
        engine.addSystem(new CoinSystem(1));
        engine.addSystem(new PlatformSystem(1));
        engine.addSystem(new PowerupSystem(1));
        engine.addSystem(new CollisionSystem(2));
        engine.addSystem(new MultiplayerSystem(3));

        //Adding some entities.
        int height = JumpingJack.instance.gameHeight*2;
        int width = JumpingJack.instance.gameWidth;
        int pPosX = (width/2);



        //Players
        int currentPlayer = PlayerData.getCurrentPlayer();
        engine.addEntity(EntityBuilder.makePlayer(pPosX - ((float)JumpingJack.instance.assetManager.getImage(PlayerComponent.player_types.get(currentPlayer)).getWidth() / 2), (float)(height *0.35), currentPlayer));




        //Starting platforms
        int platTypeCount = PlatformSystem.platform_types.size();
        for(int i=1; i<=10; i++) {
            int nextPlatType = rand.nextInt(platTypeCount) + 1;
            if(i == 3) {
                //3rd platform should always be a normal one spawned in the middle. this is to make sure the player does not lose the game immediately
                engine.addEntity(EntityBuilder.makePlatform((float)width/2 - (float)JumpingJack.instance.assetManager.getImage("greenplatform").getWidth() / 2, (float)(height * 0.3)));
            } else {
                engine.addEntity(EntityBuilder.makePlatform(rand.nextInt(width - JumpingJack.instance.assetManager.getImage(PlatformSystem.platform_types.get(nextPlatType)).getWidth()), (height * ((float)i/10)), nextPlatType));
            }
        }

        //starting powerup (higher number = less chance of spawning)
        int powerupsCount = PowerupSystem.powerup_types.size();
        double randNum = rand.nextDouble();
        double lastCond = 0;
        for(int i=powerupsCount; i>0; i--) {
            double newCond = (1 / Math.pow(i, 2)) + lastCond;
            if(randNum < newCond) {
                engine.addEntity(EntityBuilder.makePowerup(rand.nextInt(width - JumpingJack.instance.assetManager.getImage(PowerupSystem.powerup_types.get(i).get(0)).getWidth()), 800, i));
                break;
            }
            lastCond = newCond;
        }


        //starting coin
        engine.addEntity(EntityBuilder.makeCoin(150,1000));
    }

    //checking if a player is dead
    private boolean isDead(Entity entity){
        PlayerComponent player = plm.get(entity);
        return player.deadState;
    }

    public static class ScorePair implements Comparable {
        public String name;
        public int score;
        public ScorePair(String name, int score) {
            this.name = name;
            this.score = score;
        }

        @Override
        public int compareTo(Object o) {
            return ((ScorePair)o).score - this.score;
        }
    }
    //updates the score on screen
   private void updateScore(){
        // Update the current scores of the active players.
        ArrayList<ScorePair> scores = new ArrayList<>();
        entities = engine.getEntitiesFor(Family.all(PlayerComponent.class).get());
        // Get score of local player.
        for(Entity entity : engine.getEntitiesFor(Family.all(PlayerComponent.class).get())){
             PlayerComponent pl = plm.get(entity);
             //view.scoreLabel.setText(String.format("%06d", pl.score));
             ScorePair score = new ScorePair(MultiplayerSystem.name, pl.score);
             scores.add(score);
        }
        // Get scores of active multiplayer players.
        for(Entity entity : engine.getEntitiesFor(Family.all(MultiplayerComponent.class).get())){
            MultiplayerComponent pl = mplm.get(entity);
            //view.scoreLabel.setText(String.format("%06d", pl.score));
            ScorePair score = new ScorePair(pl.name, pl.score);
            scores.add(score);
        }
        Collections.sort(scores); // Sort the scores in descending order.
        String scoreText = "";
        for (ScorePair score : scores) {
            scoreText += String.format("%s: %06d\n",score.name, score.score);
        }
        // Add score of active players to scoreLabel
        view.setScoreLabel(scoreText);

        // Update the highscore list for the whole room.
       for(Entity entity : engine.getEntitiesFor(Family.all(MultiplayerScoreComponent.class).get())){
           MultiplayerScoreComponent scoreComponent = mscorm.get(entity);
           //view.scoreLabel.setText(String.format("%06d", pl.score));
           scoreText = "Highscore\n";
           int i = 0;
           for (ScorePair score : scoreComponent.scores) {
               i++;
               scoreText += String.format("%d. %s: %06d\n", i, score.name, score.score);
               if (i >= 5) {
                   break;
               }
           }
           // Add highscore list for the whole room to highscorelabel.
           view.highScoreLabel.setText(scoreText);

       }
   }

   private void checkHighScore(Entity entity){
        PlayerComponent player = plm.get(entity);
        if(!MyPreferences.isHardMode() && player.score > MyPreferences.getHighScore("easy")){
            //sets the value to be the players score
            MyPreferences.setHighScore("easy", player.score);
        }
        else if(MyPreferences.isHardMode() && player.score > MyPreferences.getHighScore("hard")){
            MyPreferences.setHighScore("hard", player.score);
       }
   }



   //if the player is dead return to highscore view/gameover view
    private void gameOver(){
        entities = engine.getEntitiesFor(Family.all(PlayerComponent.class).get());
        for(int i = 0; i < entities.size(); i++){
            Entity player = entities.get(i);
            PlayerComponent p = plm.get(player);
            if (player.getComponent(PlayerComponent.class) != null && isDead(player)) {
                checkHighScore(player);
                int score = MyPreferences.getHighScore("easy");
                if(MyPreferences.isHardMode()){
                    score = MyPreferences.getHighScore("hard");
                }
                game.setScreen(new GameOverView(game, p.score, score));
                view.dispose();
                // Clears out events and callbacks.
                JumpingJack.instance.eventManager.clear();
            }
        }
    }

    //sets the state of the game
    public void setGameState(State s){
        this.state = s;
    }

    //Switch state if the user push the pause button
    public void handleInput(){
        switch (state){
            case RUN:
                setGameState(State.PAUSE);
                break;
            case RESUME:
                game.setScreen(view);
                break;
        }
        return;
    }


    // Called every frame. Updates systems, etc.
    public void update(float delta) {
        switch (state){
            case RUN:
                engine.update(delta);
                //check if it's gameover
                updateScore();
                gameOver();
                break;
            case PAUSE:
                //kunne blitt gjort med changescreen også, men er bare fra denne klassen dette skal kunne gjøres, så er kanskje likegreit å ha det sånn det er nå
                game.setScreen(new PauseView(this, game, MyPreferences.isHardMode()));
                break;
            case RESUME:
                setGameState(GamePresenter.State.RUN);
                break;
        }

    }
    // Called when window resizes.
    public void resize(int width, int height) {
        renderSystem.resize(width, height);
    }


}
