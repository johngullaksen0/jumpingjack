package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;

import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.ecs.components.RenderComponent;
import gruppe24.progark.jumpingjack.ecs.systems.LevelSystem;
import gruppe24.progark.jumpingjack.model.MyPreferences;
import gruppe24.progark.jumpingjack.model.PlayerData;
import gruppe24.progark.jumpingjack.ui.view.SettingsView;

public class SettingsPresenter extends BasePresenter {


    public SettingsPresenter(Game game, SettingsView settingsView) {
        super(game, settingsView);
        settingsView.setCurrentBackground(PlayerData.getCurrentBackground());
        for(int i = 1; i <= PlayerData.backgrounds.size(); i ++){
            settingsView.addBackgrounds(i, JumpingJack.instance.assetManager.getImage(PlayerData.backgrounds.get(i)));
            settingsView.addThemes(PlayerData.backgrounds.get(i).toUpperCase());
        }
    }

    public void handleInput(State state){
        changeScreen(state);
    }

    public void setDificulity(boolean mode){
        LevelSystem.hardmode = mode;
        MyPreferences.setHardMode(mode);
    }

    public boolean isHardMode(){
        return MyPreferences.isHardMode();
    }

    public void resetTutorial() {
        MyPreferences.setFirstTimePlaying(true);
    }
    
    public void setBackground(int background, Texture texture){
        PlayerData.setCurrentBackground(background, texture);
    }




}
