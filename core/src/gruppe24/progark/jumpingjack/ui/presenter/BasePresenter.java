package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import gruppe24.progark.jumpingjack.ecs.systems.LevelSystem;
import gruppe24.progark.jumpingjack.model.MyPreferences;
import gruppe24.progark.jumpingjack.model.PlayerData;
import gruppe24.progark.jumpingjack.ui.view.BaseView;
import gruppe24.progark.jumpingjack.ui.view.GameView;
import gruppe24.progark.jumpingjack.ui.view.HighScoreView;
import gruppe24.progark.jumpingjack.ui.view.MainMenuView;
import gruppe24.progark.jumpingjack.ui.view.MultiplayerView;
import gruppe24.progark.jumpingjack.ui.view.SettingsView;
import gruppe24.progark.jumpingjack.ui.view.TutorialView;


public abstract class BasePresenter {


    protected Game game;

    //litt usikker på om vi trenger disse
    /*private GamePresenter gamePresenter;
    private MainMenuPresenter mainMenuPresenter;
    private HighScorePresenter highScorePresenter;*/

    public BaseView view;

    //spritebatch er egentlig nor vi ikke burde ha lokalt, men heller ha en global for hele spillet, tar mye Ram
    // SpriteBach kan tenkes på som en kontainer som holder på alle bildene og texture, og tegner alt til skjermen når vi kaller den
    public SpriteBatch batch;

    public enum State{
        MAINMENU,
        GAME,
        MULTIPLAYER,
        SETTINGS,
        HIGHSCORE,
        GAMEOVER,
        RUN,
        RESUME,
        PAUSE,
        TUTORIAL
    }

    public BasePresenter(Game game, BaseView view){
        this.game = game;
        this.view = view;
        this.view.setBackground(PlayerData.getBackground());
        this.batch = new SpriteBatch();

    }

    public void changeScreen(State screen){
        switch (screen){
            case MAINMENU:
                //if(menuScreen == null) menuScreen = new MenuScreen();
                game.setScreen(new MainMenuView(game));
                view.dispose();
                break;
            case GAME:
                //if(gamePresenter == null) gamePresenter = new GamePresenter();
                if (MyPreferences.isFirstTimePlaying()) {
                    changeScreen(State.TUTORIAL);
                    view.dispose();;
                    break;
                }
                game.setScreen(new GameView(game));
                LevelSystem.hardmode = MyPreferences.isHardMode();
                view.dispose();
                this.dispose();
                break;
            case HIGHSCORE:
                //if(highScorePresenter == null) highScorePresenter = new HigScorePresenter();
                game.setScreen(new HighScoreView(game));
                view.dispose();
                this.dispose();
                break;
            case MULTIPLAYER:
                game.setScreen(new MultiplayerView(game));
                LevelSystem.hardmode = MyPreferences.isHardMode();
                view.dispose();
                this.dispose();
                break;
            case SETTINGS:
                game.setScreen(new SettingsView(game));
                view.dispose();
                this.dispose();
                break;
            case GAMEOVER:
                // game.setScreen(new GameOverView(game, score,  highScore));
                break;
            case TUTORIAL:
                game.setScreen(new TutorialView(game));
                break;
        }
    }

    public void handleMusic(boolean checkbox){
        MyPreferences.setMusicEnabled(checkbox);
        if(MyPreferences.isMusicEnabled()) {
            PlayerData.turnOnMusic();
        }
        else {
            PlayerData.turnOfMusic();
        }
    }

    public boolean isMusicEnabled(){
        return MyPreferences.isMusicEnabled();
    }

    public void dispose() {
        batch.dispose();
    }


}