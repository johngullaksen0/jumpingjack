package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import gruppe24.progark.jumpingjack.ecs.systems.MultiplayerSystem;
import gruppe24.progark.jumpingjack.ui.view.BaseView;


public class MultiplayerPresenter extends BasePresenter {


    private String room = "noRoom";
    private String name = "noName";

    public MultiplayerPresenter(Game game, BaseView view) {
        super(game, view);
    }

    public void connectToRoom() {
        if (Gdx.app.getType() != Application.ApplicationType.Android) {
            System.out.println("Multiplayer is only supported on Android, not " + Gdx.app.getType());
            changeScreen(State.MAINMENU);
            return;
        }
        Input.TextInputListener listenerRoom = new Input.TextInputListener() {

            @Override
            public void input(String text) {
                room = text;
                Input.TextInputListener listenerName = new Input.TextInputListener() {

                    @Override
                    public void input(String text) {
                        name = text;
                        // Set the player name and the room name.
                        MultiplayerSystem.name = name;
                        MultiplayerSystem.room = room;

                        changeScreen(State.GAME);
                    }

                    @Override
                    public void canceled() {
                        changeScreen(State.MAINMENU);
                    }
                };
                Gdx.input.getTextInput(listenerName, "Write in the name you want to use", "testbert", "write the name here");
            }

            @Override
            public void canceled() {
                changeScreen(State.MAINMENU);
            }
        };
        Gdx.input.getTextInput(listenerRoom, "Write in the name of the multiplayer room", "room1", "write the name here");
    }

}
