package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Game;

import gruppe24.progark.jumpingjack.model.MyPreferences;

import gruppe24.progark.jumpingjack.ui.view.BaseView;
import gruppe24.progark.jumpingjack.ui.view.TutorialView;

public class TutorialPresenter extends BasePresenter {

    private TutorialView view;

    public TutorialPresenter(Game game, BaseView view) {
        super(game, view);

        this.view = (TutorialView) view;
    }

    public void handleInput() {
        MyPreferences.setFirstTimePlaying(false);
        changeScreen(State.GAME);
    }
}
