package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Game;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.systems.MultiplayerSystem;
import gruppe24.progark.jumpingjack.model.PlayerData;
import gruppe24.progark.jumpingjack.ui.view.MainMenuView;

public class MainMenuPresenter extends BasePresenter {


    public MainMenuPresenter(Game game, MainMenuView mainMenuView) {
        super(game, mainMenuView);
        //mainMenuView.setBackground(PlayerData.getBackground());
        mainMenuView.setCurrentCharacter(PlayerData.getCurrentPlayer());
        for(int i = 1; i <= PlayerComponent.player_types.size(); i ++){
            mainMenuView.addCharacters(i, JumpingJack.instance.assetManager.getImage(PlayerComponent.player_types.get(i)));
        }

    }
    public void setSinglePlayer(){
        MultiplayerSystem.room = ""; // Set room to nothing, makes the game play in singleplayer.
    }

    public void handleInput(State state){
        changeScreen(state);
    }

    public void changeCharacter(int player){
        PlayerData.setCurrentPlayer(player);
    }

}
