package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Game;

import gruppe24.progark.jumpingjack.model.PlayerData;
import gruppe24.progark.jumpingjack.ui.view.BaseView;


public class GameOverPresenter extends BasePresenter {
    public GameOverPresenter(Game game, BaseView view) {
        super(game, view);
      //  this.view.setBackground(PlayerData.getBackground());
    }


    public void handleInput(State state) {
        changeScreen(state);
        view.dispose();
    }
}
