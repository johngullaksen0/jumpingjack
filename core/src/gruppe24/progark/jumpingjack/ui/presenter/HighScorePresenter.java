package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Game;
import gruppe24.progark.jumpingjack.model.MyPreferences;
import gruppe24.progark.jumpingjack.model.PlayerData;
import gruppe24.progark.jumpingjack.ui.view.HighScoreView;


public class HighScorePresenter extends BasePresenter {


    //henter inn spillet fra MyGdxGame og viet fra HighScore view for å lage en connection mellom dem
    public HighScorePresenter(Game game, HighScoreView view){
        super(game, view);
      //  this.view.setBackground(PlayerData.getBackground());
    }

    public int getHighScore(String mode){
        return MyPreferences.getHighScore(mode);
    }


    //Metode for å håndtere input fra highscore view, endrer skjerm til GameView og disposer det som er tegnet til skjerm på highscore view for å frigjøre minne
    public void handleInput(State state){
        changeScreen(state);
        view.dispose();
    }

}
