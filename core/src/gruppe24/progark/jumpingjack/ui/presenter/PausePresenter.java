package gruppe24.progark.jumpingjack.ui.presenter;

import com.badlogic.gdx.Game;

import gruppe24.progark.jumpingjack.ui.view.BaseView;

public class PausePresenter extends BasePresenter {
    public PausePresenter(Game game, BaseView view) {
        super(game, view);
    }
}
