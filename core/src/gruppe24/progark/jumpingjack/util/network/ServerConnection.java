package gruppe24.progark.jumpingjack.util.network;
/*
    This class is used for connecting to the online part of the game. It can be extended and used for different platforms. Currently only extended by FirebaseServerConnection in the android module.
*/
public class ServerConnection {

    protected NetworkCallback callback;
    protected HighscoreCallback highscoreCallback;

    public ServerConnection() {

    }

    /*
        Connects to the room specified in the parameter. If the room does not exist, makes a new one instead.
    */
    public void connectToRoom(String roomName) {

    }
    /*
        Closes current room, removes listeners, etc.
    */
    public void closeRoom() {

    }

    /*
        Registers a callback for network/online play related events, such as other players' score and/or position
    */
    public void registerCallback(NetworkCallback callback) {
        this.callback = callback;
    }

    /*
        Updates the player data of this player on the remote server. MultiPlayerData is an object with position, score, name etc.
    */
    public void updatePlayerData(MultiPlayerData data) {

    }

    /*
        Registers a callback for highscore related events, callback is called when a new highscore is added. set 'onlyOnce' to true if you only want to get the highscore once, not again when it changes.
    */
    public void registerHighscoreCallback(HighscoreCallback callback, boolean onlyOnce) {
        if (!onlyOnce) this.highscoreCallback = callback;
    }
    /*
        Updates the highscore of this player on the remote server.
    */
    public void updateHighScore(String playerName, int score) {

    }

    /*
        Set in which category the highscores for this session should be saved. Example. Hardmode, Easmode, etc. Call this before connecting to a room, and registering callbacks.
     */
    public void setHighscoreLocation(String location) {

    }


}
