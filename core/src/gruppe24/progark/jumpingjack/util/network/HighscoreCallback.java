package gruppe24.progark.jumpingjack.util.network;

import java.util.HashMap;

public interface HighscoreCallback {

    public void highscoreCallback(HashMap<String, Integer> scores);
}
