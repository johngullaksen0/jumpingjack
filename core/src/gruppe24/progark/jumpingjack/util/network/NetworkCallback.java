package gruppe24.progark.jumpingjack.util.network;

import java.util.ArrayList;

public interface NetworkCallback {

    public void networkCallback(ArrayList<MultiPlayerData> players);
}
