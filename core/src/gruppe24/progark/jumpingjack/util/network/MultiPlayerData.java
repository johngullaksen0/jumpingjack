package gruppe24.progark.jumpingjack.util.network;

public class MultiPlayerData {

    public boolean connected = false;
    public String name = "testy";
    public int highscore = 0;
    public int score = 0;
    public float x = 0;
    public float y = 0;
    public float vx = 0;
    public float vy = 0;

    /*
        Helper object used for playerdata, both the local player and online players. These are the objects sent between client and server.
     */
    public MultiPlayerData() {

    }
}
