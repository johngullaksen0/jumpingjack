package gruppe24.progark.jumpingjack;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import gruppe24.progark.jumpingjack.model.PlayerData;
import gruppe24.progark.jumpingjack.ui.view.MainMenuView;

public class MyGdxGame extends Game {

	
	@Override
	public void create () {
		//Sets up singleton with neccessary data, loads files, etc.
		JumpingJack.instance.setup(new SpriteBatch());
	
		PlayerData data = new PlayerData();

		this.setScreen(new MainMenuView(this));
	}




	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
	}
}
