package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Rectangle;

import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.ecs.components.HitboxComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;

/*
    System that calculates if entities collide/overlap with eachother. Sends out events if they do.
    Any system that wishes to know/be notified of any collisions should subscribe to an 'EVENT_COLLISION' event in EventManager(JumpingJack.instance.eventManager).
 */
public class CollisionSystem extends EntitySystem {
    public static int EVENT_COLLISION = 23;

    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<HitboxComponent> hbm = ComponentMapper.getFor(HitboxComponent.class);

    public CollisionSystem(int priority) {
        super(priority);
    }

    private ImmutableArray<Entity> entities;

    @Override
    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(PositionComponent.class, HitboxComponent.class).get());
    }

    public void update(float deltaTime) {

        // Check collisions for entities with both position and hitbox.
        for (int i = 0; i < entities.size(); ++i) {
            Entity e1 = entities.get(i);
            PositionComponent position1 = pm.get(e1);
            HitboxComponent hitbox1 = hbm.get(e1);
            // Loops through all relevant entities added to the engine. Not really efficient at all...
            for (int o = 0; o < entities.size(); ++o) {
                Entity e2 = entities.get(o);
                if (e1 != e2) {
                    PositionComponent position2 = pm.get(e2);
                    HitboxComponent hitbox2 = hbm.get(e2);

                    // Creates rectangles for collision testing.
                    Rectangle rect1 = new Rectangle(position1.x, position1.y, hitbox1.width, hitbox1.height);
                    Rectangle rect2 = new Rectangle(position2.x, position2.y, hitbox2.width, hitbox2.height);
                    if (rect1.overlaps(rect2)) {
                        // Notifies subscribers of any relevant events.
                        JumpingJack.instance.eventManager.notify(EVENT_COLLISION, e1, e2);
                    }
                }
            }
        }
    }
}
