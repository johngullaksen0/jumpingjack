package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.EntitySystem;

public class LevelSystem extends EntitySystem {

    //hardmode off = player dies when out of bounds
    //hardmode on = horizontal edges now behave as walls, and the player may use them to bounce in the opposite direction
    public static boolean hardmode;
}
