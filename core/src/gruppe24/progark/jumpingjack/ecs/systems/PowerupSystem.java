package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import gruppe24.progark.jumpingjack.EntityEventCallback;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.EntityBuilder;
import gruppe24.progark.jumpingjack.ecs.components.HitboxComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.PowerupComponent;
import gruppe24.progark.jumpingjack.ecs.components.RenderComponent;
import gruppe24.progark.jumpingjack.ecs.components.VelocityComponent;

public class PowerupSystem extends IteratingSystem {
    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private ComponentMapper<HitboxComponent> hbm = ComponentMapper.getFor(HitboxComponent.class);
    private ComponentMapper<PlayerComponent> plc = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<PowerupComponent> powm = ComponentMapper.getFor(PowerupComponent.class);
    private ComponentMapper<RenderComponent> rm = ComponentMapper.getFor(RenderComponent.class);

    private int powTimer;
    private static Texture playerImg = null;
    private static Texture powImg = null;
    private ImmutableArray<Entity> entities;

    //map within map - [id["name of img", "effect (* jump speed)", "duration in seconds", "remove img on collission yes/no"],...]
    public static final HashMap<Integer, ArrayList<String>> powerup_types = new HashMap<Integer, ArrayList<String>>() {{
        ArrayList<String> pow1 = new ArrayList<String>();
        pow1.add("spring");
        pow1.add("1.5");
        pow1.add("0");
        pow1.add("yes");
        put(1, pow1);
        ArrayList<String> pow2 = new ArrayList<String>();
        pow2.add("energydrink");
        pow2.add("1.5");
        pow2.add("7");
        pow2.add("yes");
        put(2, pow2);
        ArrayList<String> pow3 = new ArrayList<String>();
        pow3.add("portal");
        pow3.add("5");
        pow3.add("1");
        pow3.add("yes");
        put(3, pow3);
        ArrayList<String> pow4 = new ArrayList<String>();
        pow4.add("jetpack");
        pow4.add("15");
        pow4.add("5");
        pow4.add("no");
        put(4, pow4);
    }};

    public PowerupSystem(int priority) {
        super(Family.all(PositionComponent.class, VelocityComponent.class, PowerupComponent.class, HitboxComponent.class).get(), priority);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        entities = engine.getEntities();
        // Listen for added powerup entities. When they are added subscribe to their collision events, unsubscribe when they are removed.
        Family family = Family.all(PositionComponent.class, VelocityComponent.class, PowerupComponent.class).get();
        engine.addEntityListener(family, new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                JumpingJack.instance.eventManager.subscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
                powTimer = 0;
            }

            @Override
            public void entityRemoved(Entity entity) {
                JumpingJack.instance.eventManager.unsubscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
            }
        });
    }

    private EntityEventCallback collisionCallback = new EntityEventCallback() {
        @Override
        public void entityEventCallback(Entity e1, Entity e2) {
            if (e2.getComponent(PlayerComponent.class) != null) {
                PowerupComponent pow = powm.get(e1);
                VelocityComponent powVel = vm.get(e1);

                VelocityComponent vel = vm.get(e2);
                PlayerComponent play = plc.get(e2);

                //boolean to check if player is currently affected by a powerup
                //true on collision
                if(!pow.isActive) {
                    pow.isActive = true;
                }

                /*  how the powerups affect the player
                    specify specific boosts for each (i.e. jetpack/portal)
                    powerups with duration = 0 is an instant boost
                    powerups with effects not explicitly stated have defaulted to increase jumpspeed times their jumpmultiplier for their duration
                 */
                if(powerup_types.get(pow.type).get(0).equals("jetpack") ) {
                    vel.y = play.origJumpSpeed * pow.jumpMultiplier;
                } else if (powerup_types.get(pow.type).get(0).equals("portal")) {
                    vel.y = play.origJumpSpeed * pow.jumpMultiplier;
                } else if (pow.duration == 0) {
                    vel.y = play.origJumpSpeed * pow.jumpMultiplier;
                } else if (pow.duration > 0) {
                    play.jumpSpeed = play.origJumpSpeed * pow.jumpMultiplier;
                }
            }
        }
    };

    @Override
    public void processEntity(Entity entity, float deltaTime) {

        //makes sure that the powerups' position stays relative to the game speed
        VelocityComponent vel = vm.get(entity);
        if (vel.y != EntityBuilder.gameSpeed) {
            vel.y = EntityBuilder.gameSpeed;
        }

        PowerupComponent pow = powm.get(entity);
        HitboxComponent hbox = hbm.get(entity);
        PositionComponent pos = pm.get(entity);
        RenderComponent rend = rm.get(entity);


        for(Entity playerEntity : entities) {
            if(playerEntity.getComponent(PlayerComponent.class) != null) {
                PlayerComponent player = plc.get(playerEntity);
                VelocityComponent playerVel = vm.get(playerEntity);
                PositionComponent playerPos = pm.get(playerEntity);
                HitboxComponent playerHitbox = hbm.get(playerEntity);
                RenderComponent playerRend = rm.get(playerEntity);

                //if currently afffecting the player
                if(pow.isActive) {
                    pow.time += Gdx.graphics.getDeltaTime();

                    //if render to be removed on collission
                    if(powerup_types.get(pow.type).get(3).equals("yes")) {
                        if(entity.getComponent(RenderComponent.class) != null) {
                            //saves img for later use, needed for portal
                            powImg = rend.img;
                            entity.remove(RenderComponent.class);
                        }
                    }

                    //code specific for each powerup, needed for powerups that keeps an image after collision
                    if(powerup_types.get(pow.type).get(0).equals("jetpack")) {
                        vel.y = 0;
                        pos.x = playerPos.x + ((float) (playerHitbox.width - hbox.width) / 2);
                        pos.y = playerPos.y + ((float) (playerHitbox.height - hbox.height) / 2);
                        //budget animation
                        if(powTimer % 4 == 0) {
                            rend.img = JumpingJack.instance.assetManager.getImage("jetpack1");
                        } else if (powTimer % 4 == 2) {
                            rend.img = JumpingJack.instance.assetManager.getImage("jetpack2");
                        }
                    } else if(powerup_types.get(pow.type).get(0).equals("portal")) {
                        if(playerEntity.getComponent(RenderComponent.class) != null) {
                            //saves img for later use, needed for portal
                            playerImg = playerRend.img;
                            //removing the render component from the player so that the player is invisible while in portal
                            playerEntity.remove(RenderComponent.class);
                        }
                        pos.y = playerPos.y;
                    }
                } else if (pos.y < (0 - hbox.height)) {
                    //if powerup not active and out of lower bounds -> remove from engine and schedule a new spawn
                    pow.time = 0;
                    resetValues(playerEntity, entity);
                }

                //if current time the powerup has lasted is larger than its full duration
                if(pow.time > pow.duration) {
                    //unsubs from collisions
                    JumpingJack.instance.eventManager.unsubscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
                    if (powerup_types.get(pow.type).get(0).equals("portal") ) {
                        //the duration is over and portal's render == null -> reinstate the portal and make the player jump out of it for some sick gameplay
                        if(entity.getComponent(RenderComponent.class) == null) {

                            //playing exiting portal at about 40% from top of screen, above the portal
                            playerPos.y = (float)(JumpingJack.instance.gameHeight * .6);
                            pos.y = playerPos.y;
                            pos.x = playerPos.x + ((float) (playerHitbox.width - hbox.width) / 2);

                            //re-applying render component to player and power up entity when exiting portal
                            RenderComponent newPowRend = new RenderComponent();
                            newPowRend.img = powImg;
                            entity.add(newPowRend);

                            RenderComponent newPlayerRend = new RenderComponent();
                            newPlayerRend.img = playerImg;
                            playerEntity.add(newPlayerRend);

                            //player jumps out of portal with normal jumpspeed
                            playerVel.y = player.origJumpSpeed;
                            //portal is immediately following the games speed
                            vel.y = EntityBuilder.gameSpeed;
                        }
                    } else {
                        //for powerups that duration has ended and is NOT a portal, reset alterede stuff and schedule a new spawn (retValues)
                        pow.time = 0;
                        resetValues(playerEntity, entity);
                    }
                    //regardless of the type of powerup, when the duration of the boost has ended it is set inactive.
                    //atm this is only affects the portal, however more powerups may be implemented later
                    pow.isActive = false;
                }
                break;
            }
        }
        powTimer++;
    }

    //reverting altered player values (only jumpspeed needeed atm)
    //removing powerup from engine and calls for a new spawn
    private void resetValues(Entity player, Entity powerup) {
        PlayerComponent pl = plc.get(player);
        pl.jumpSpeed = pl.origJumpSpeed;
        getEngine().removeEntity(powerup);
        schedulePowerup();
    }

    //schedules spawnPowerUp to be called in 5-10 sec (random)
    private void schedulePowerup() {
        Random rand = new Random();
        int timeUntilNextPowerup = rand.nextInt((10 - 5) + 1) + 5;
        Timer.schedule(new Timer.Task(){
                           @Override
                           public void run() {
                               spawnPowerup();
                           }
                       }
                , timeUntilNextPowerup);
        System.out.println("NEW IN : " + timeUntilNextPowerup);
    }

    private void spawnPowerup() {
        Random rand = new Random();
        float ypos = JumpingJack.instance.gameHeight*2.1f;

        //amount of different powerups
        int powerupsCount = powerup_types.size();

        double randNum = rand.nextDouble();
        double lastCond = 0;
        /*
        spawn chance for powerups:
        powerup number 4 = 1/4^2 = 6.25%
        power number 3 = 1/3^2 = 11.1%
        powerup number 2 = 1/2^2 = 25%
        powerup 1 = 100% - 6.25% - 11.1% - 25% = 57.65%
        if a fifth powerup were to be added, its chance of spawning would be 1/5^2 = 1/25 = 4%. that would only affect the first powerup, which would lose 4% chance.
        */
        for(int i=powerupsCount; i>0; i--) {
            double newCond = (1 / Math.pow(i, 2)) + lastCond;
            if(randNum < newCond) {
                getEngine().addEntity(EntityBuilder.makePowerup(rand.nextInt(JumpingJack.instance.gameWidth - JumpingJack.instance.assetManager.getImage(powerup_types.get(i).get(0)).getWidth()), ypos, i));
                break;
            }
            lastCond = newCond;
        }
    }

}
