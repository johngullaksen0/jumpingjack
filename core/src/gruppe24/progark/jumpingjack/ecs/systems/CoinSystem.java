package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.utils.Timer;

import java.util.ArrayList;
import java.util.Random;

import gruppe24.progark.jumpingjack.EntityEventCallback;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.EntityBuilder;
import gruppe24.progark.jumpingjack.ecs.components.CoinComponent;
import gruppe24.progark.jumpingjack.ecs.components.HitboxComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.RenderComponent;
import gruppe24.progark.jumpingjack.ecs.components.VelocityComponent;

public class CoinSystem extends IteratingSystem {
    private ComponentMapper<CoinComponent> coin = ComponentMapper.getFor(CoinComponent.class);
    private ComponentMapper<PlayerComponent> player = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private ComponentMapper<HitboxComponent> hbm = ComponentMapper.getFor(HitboxComponent.class);
    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<RenderComponent> rend = ComponentMapper.getFor(RenderComponent.class);

    private static int coinTimer;
    private static int currentCoin;

    //the different images for the coin animation. names from AssetManager
    private ArrayList<String> coinImg = new ArrayList<String>() {
        {
            add("coin1");
            add("coin2");
            add("coin3");
        }
    };



    public CoinSystem(int priority) {
        super(Family.all(PositionComponent.class, VelocityComponent.class, CoinComponent.class, HitboxComponent.class).get(), priority);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        // Listen for added platform entities. When they are added subscribe to their collision events, unsubscribe when they are removed.
        Family family = Family.all(PositionComponent.class, VelocityComponent.class, CoinComponent.class).get();
        engine.addEntityListener(family, new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                JumpingJack.instance.eventManager.subscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
                coinTimer = 0;
                currentCoin = 0;
            }

            @Override
            public void entityRemoved(Entity entity) {
                JumpingJack.instance.eventManager.unsubscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
            }
        });
    }

    private EntityEventCallback collisionCallback = new EntityEventCallback() {
        @Override
        public void entityEventCallback(Entity e1, Entity e2) {
            if (e2.getComponent(PlayerComponent.class) != null) {
                CoinComponent coinValue = coin.get(e1);
                PlayerComponent pl = player.get(e2);
                //updates both '|'score' and 'velocityscore'
                //velocityscore is varying and may also decrease; score is set to velocityscore for each new max
                //updates both to instantly reward the player
                pl.score += coinValue.value;
                pl.velocityScore += coinValue.value;
                //schedules a new coin spawn on collision
                scheduleCoinSpawn(e1);

            }
        }
    };

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        coinTimer++;
        //budget animation
        if(coinTimer % 7 == 0) {
            //updates animation every 7 dt
            RenderComponent render = rend.get(entity);
            if (currentCoin == 3) {
                render.img = JumpingJack.instance.assetManager.getImage(coinImg.get(0));
                currentCoin = 0;
            } else {
                render.img = JumpingJack.instance.assetManager.getImage(coinImg.get(currentCoin));
                currentCoin ++;
            }

        }

        PositionComponent position = pm.get(entity);
        HitboxComponent hbox = hbm.get(entity);
        //schedules new spawn when current coin out of lower bounds
        if(position.y < (0 - hbox.height)) {
            scheduleCoinSpawn(entity);
        }

        //following the games speed to make sure that the coins relative position is the same
        VelocityComponent vel = vm.get(entity);
        if (vel.y != EntityBuilder.gameSpeed) {
            vel.y = EntityBuilder.gameSpeed;
        }
    }

    //schedules a new spawn to be called in 2-7 sec (random)
    private void scheduleCoinSpawn(Entity entity) {
        getEngine().removeEntity(entity);
        Random rand = new Random();
        int timeUntilNextCoin = rand.nextInt((7 - 2) + 1) + 2;
        Timer.schedule(new Timer.Task(){
                           @Override
                           public void run() {
                               spawnCoin();
                           }
                       }
                , timeUntilNextCoin);
    }

    private void spawnCoin() {
        Random rand = new Random();
        float ypos = JumpingJack.instance.gameHeight*2.1f;
        getEngine().addEntity(EntityBuilder.makeCoin(rand.nextInt(JumpingJack.instance.gameWidth - JumpingJack.instance.assetManager.getImage("coin1").getWidth()), ypos, 1000));

    }
}
