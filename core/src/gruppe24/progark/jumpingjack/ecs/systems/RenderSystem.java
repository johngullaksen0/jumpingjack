package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.RenderComponent;
import gruppe24.progark.jumpingjack.model.PlayerData;

/*
    Draws entities with a RenderComponent to the screen.
 */
public class RenderSystem extends EntitySystem {
    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<RenderComponent> vm = ComponentMapper.getFor(RenderComponent.class);

    private ImmutableArray<Entity> entities;

    SpriteBatch batch;

    // Objects used for drawing to the screen.
    private FrameBuffer m_fbo = null;
    private TextureRegion m_fboRegion = null;

    public RenderSystem(int priority) {
        super(priority);
    }

    @Override
    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(PositionComponent.class, RenderComponent.class).get());
        batch = new SpriteBatch();
    }

    @Override
    public void removedFromEngine(Engine engine) {
        batch.dispose();
    }

    @Override
    public void update(float deltaTime) {
        // Clear screen.
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Get the Width and Height of the world
        int width = JumpingJack.instance.gameWidth;
        int height = JumpingJack.instance.gameHeight;
        // Calculate
        float ratio = (float)Gdx.graphics.getWidth() / width;
        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();

        //Scale the screen to fit screen of device. Draws to a texture before drawing to the screen.
        if(m_fbo == null)
        {
            m_fbo = new FrameBuffer(Pixmap.Format.RGB565, width, height, false);
            m_fboRegion = new TextureRegion(m_fbo.getColorBufferTexture());
            m_fboRegion.flip(false, true);
        }
        m_fbo.begin();

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw every entity with RenderComponent.
        batch.begin();
        batch.draw(JumpingJack.instance.assetManager.getImage(PlayerData.backgrounds.get(PlayerData.getCurrentBackground())+"background"), 0, 0, width, height*ratio);
        Entity playerEntity = null;
        for (int i = 0; i < entities.size(); ++i) {
            Entity entity = entities.get(i);
            if(entity.getComponent(PlayerComponent.class) == null) {
                PositionComponent position = pm.get(entity);
                RenderComponent render = vm.get(entity);
                batch.draw(render.img, position.x, position.y);
            } else {
                playerEntity = entity;
            }
        }

        //draw player last to be in the foreground of objects
        if(playerEntity != null) {
            PositionComponent position = pm.get(playerEntity);
            RenderComponent render = vm.get(playerEntity);
            batch.draw(render.img, position.x, position.y);
        }

        batch.end();

        // Draw buffer to screen. Scales buffer/texture to the screen size of the device used.
        if (m_fbo != null) {
            m_fbo.end();

            batch.begin();
            batch.draw(m_fboRegion, 0, 0, 0, 0, screenWidth, screenHeight, ratio, ratio,0);
            batch.end();
        }

    }

    /*
        Called when the screen is resized. TODO delete?
     */
    public void resize(int width, int height) {
        //gamePort.update(width, height);
        //gamecam.position.set(gamecam.viewportWidth/2,gamecam.viewportHeight/2,0);
    }
}
