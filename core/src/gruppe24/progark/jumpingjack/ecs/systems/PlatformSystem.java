package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import gruppe24.progark.jumpingjack.EntityEventCallback;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.EntityBuilder;
import gruppe24.progark.jumpingjack.ecs.components.HitboxComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlatformComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.VelocityComponent;

public class PlatformSystem extends IteratingSystem {
    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private ComponentMapper<PlatformComponent> plm = ComponentMapper.getFor(PlatformComponent.class);
    private ComponentMapper<HitboxComponent> hbm = ComponentMapper.getFor(HitboxComponent.class);
    private ComponentMapper<PlayerComponent> plc = ComponentMapper.getFor(PlayerComponent.class);

    //hashmap of the different platforms, values is the name from AssetManager
    public final static Map<Integer, String> platform_types = new HashMap<Integer, String>() {{
        put(1, "greenplatform");
        put(2, "redplatform");
        put(3, "yellowplatform");
        put(4, "blueplatform");
        put(5, "blackplatform");

    }};

    public PlatformSystem(int priority) {
        super(Family.all(PositionComponent.class, VelocityComponent.class, PlatformComponent.class, HitboxComponent.class).get(), priority);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        // Listen for added platform entities. When they are added subscribe to their collision events, unsubscribe when they are removed.
        Family family = Family.all(PositionComponent.class, VelocityComponent.class, PlatformComponent.class).get();
        engine.addEntityListener(family, new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                JumpingJack.instance.eventManager.subscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
            }

            @Override
            public void entityRemoved(Entity entity) {
                JumpingJack.instance.eventManager.unsubscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
            }
        });
    }
    /*
        Called when a player collides with another entity.
     */
    private EntityEventCallback collisionCallback = new EntityEventCallback() {
        @Override
        public void entityEventCallback(Entity e1, Entity e2) {
            if (e2.getComponent(PlayerComponent.class) != null) {
                //logic for the platforms when player collides
                PlatformComponent p = plm.get(e1);
                PositionComponent pos = pm.get(e1);
                PositionComponent pos2 = pm.get(e2);
                VelocityComponent vel = vm.get(e2);
                PlayerComponent player = plc.get(e2);

                //red platforms can only handle one collision, and is therefore removed
                //removing it from engine and initializes new spawn
                if (p.type == PlatformComponent.TYPE_RED && pos2.y >= pos.y && vel.y == player.jumpSpeed ) {
                    respawnPlatform(e1);
                    getEngine().removeEntity(e1);
                }
            }
        }
    };

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        PositionComponent position = pm.get(entity);
        HitboxComponent hbox = hbm.get(entity);
        //respawns platforms when they get out of lower bounds
        if(position.y < (0 - hbox.height)) {
            respawnPlatform(entity);
            getEngine().removeEntity(entity);
        }

        //the player may increase the gameSpeed when they reach a high vertical velocity
        //this makes sure that the platforms always follow the current speed of the game
        VelocityComponent vel = vm.get(entity);
        if (vel.y != EntityBuilder.gameSpeed) {
            vel.y = EntityBuilder.gameSpeed;
        }
    }

    /*
        Used to spawn new platforms.
     */
    private void respawnPlatform(Entity entity) {
        Random rand = new Random();
        PositionComponent pos = pm.get(entity);

        float ypos = pos.y + JumpingJack.instance.gameHeight*2.1f;

        //spawns a random platform, all with equal chance
        int platTypeCount = platform_types.size();
        int nextPlat = rand.nextInt(platTypeCount) + 1;
        getEngine().addEntity(EntityBuilder.makePlatform(rand.nextInt(JumpingJack.instance.gameWidth - JumpingJack.instance.assetManager.getImage(platform_types.get(nextPlat)).getWidth()), ypos, nextPlat));

    }
}
