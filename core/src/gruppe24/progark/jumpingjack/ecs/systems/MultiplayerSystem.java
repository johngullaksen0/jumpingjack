package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import gruppe24.progark.jumpingjack.EntityEventCallback;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.ecs.EntityBuilder;
import gruppe24.progark.jumpingjack.ecs.components.MultiplayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.MultiplayerScoreComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.VelocityComponent;
import gruppe24.progark.jumpingjack.ui.presenter.GamePresenter;
import gruppe24.progark.jumpingjack.util.network.HighscoreCallback;
import gruppe24.progark.jumpingjack.util.network.MultiPlayerData;
import gruppe24.progark.jumpingjack.util.network.NetworkCallback;
import gruppe24.progark.jumpingjack.util.network.ServerConnection;

/*
    System used for multiplayer functionality. Uses a Serverconnection, only implemented on android.
 */
public class MultiplayerSystem extends IntervalSystem {

    // object used for connecting to gameserver.
    public static ServerConnection connection;
    // the name of the room to connect to.
    public static String room = "room1";
    // the name of the player. Used for highscore.
    public static String name = "player";

    private ArrayList<Entity> playerEntities = new ArrayList<>();

    private HashMap<String, Entity> multiPlayers = new HashMap<>();
    private Engine engine;

    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private ComponentMapper<PlayerComponent> plm = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<MultiplayerComponent> mplm = ComponentMapper.getFor(MultiplayerComponent.class);

    public MultiplayerSystem(int priority) {
        //Set interval of updates, first parameter is updates information in server every second. 1 is once a second, 0.5f is every half second.
        super(0.5f, priority);
    }
    @Override
    public void addedToEngine(final Engine engine) {
        this.engine = engine;
        Family family = Family.all(PositionComponent.class, VelocityComponent.class, PlayerComponent.class).get();

        // Checks if a room is specified, if not, play in singleplayer.
        if (room.length() < 1) {
            return;
        }

        // Add an entity with a MutiplayerScore. Used to save the score of other players so that other systems can access it.
        Entity entity = new Entity();
        entity.add(new MultiplayerScoreComponent());
        engine.addEntity(entity);

        //Listens for added players.
        engine.addEntityListener(family, new EntityListener() {
            @Override
            public void entityAdded(final Entity entity) {
                playerEntities.add(entity);
                // Listen for the death of the player. Send data back to server, connected status and new highscore.
                JumpingJack.instance.eventManager.subscribe(entity, PlayerSystem.EVENT_PLAYER_DEATH, new EntityEventCallback() {
                    @Override
                    public void entityEventCallback(Entity e1, Entity e2) {
                        MultiPlayerData mpd = makeMultiplayerData(e1);
                        mpd.connected = false;
                        connection.updatePlayerData(mpd); // Update connection status
                        if (mpd.score > mpd.highscore) {
                            connection.updateHighScore(mpd.name, mpd.score); // Update high score.
                            System.out.println("DEAAATH!!!!");
                            connection.closeRoom();
                        }
                    }
                });
                // Check if the player has a highscore in this room.
                connection.registerHighscoreCallback(new HighscoreCallback() {
                    @Override
                    public void highscoreCallback(HashMap<String, Integer> scores) {
                        if (scores.containsKey(name)) {
                            plm.get(entity).highscore = scores.get(name);
                        }
                    }
                }, true);
            }

            @Override
            public void entityRemoved(Entity entity) {
                playerEntities.remove(entity);
            }
        });

        // Check for difficulty. Different highscorelists for different difficulties.
        if (LevelSystem.hardmode) {
            connection.setHighscoreLocation("highscores");
        }
        else {
            connection.setHighscoreLocation("highscoreeasy");
        }

        //Register callback for network related events.
        connection.registerCallback(new NetworkCallback() {
            @Override
            public void networkCallback(ArrayList<MultiPlayerData> players) {
                for (MultiPlayerData player : players) {
                    if (player.name.equals(name)) continue;
                    if (multiPlayers.containsKey(player.name)) {

                        Entity e = multiPlayers.get(player.name);
                        if (!player.connected) {
                            engine.removeEntity(e);
                            multiPlayers.remove(player.name);
                        }
                        PositionComponent pc = pm.get(e);
                        VelocityComponent vc = vm.get(e);
                        MultiplayerComponent plc = mplm.get(e);

                        pc.x = player.x;
                        pc.y = player.y;
                        vc.x = player.vx;
                        //vc.y = player.vy;
                        plc.score = player.score;
                        plc.highscore = player.highscore;
                    }
                    else if (player.connected){
                        Entity e = EntityBuilder.makeMultiPlayer(player.x, player.y, player.name);
                        multiPlayers.put(player.name, e);
                        PositionComponent pc = pm.get(e);
                        VelocityComponent vc = vm.get(e);
                        MultiplayerComponent plc = mplm.get(e);

                        pc.x = player.x;
                        pc.y = player.y;
                        vc.x = player.vx;
                        //vc.y = player.vy;
                        plc.score = player.score;
                        plc.highscore = player.highscore;
                        engine.addEntity(e);
                    }
                }
            }
        });
        // Add a callback for when highscores change.
        connection.registerHighscoreCallback(new HighscoreCallback() {
            @Override
            public void highscoreCallback(HashMap<String, Integer> scores) {
                for (Entity e : engine.getEntitiesFor(Family.all(MultiplayerScoreComponent.class).get())) {
                    MultiplayerScoreComponent scoreComponent = e.getComponent(MultiplayerScoreComponent.class);
                    ArrayList<GamePresenter.ScorePair> scoreList = new ArrayList<>();
                    for (Map.Entry<String,Integer> entry : scores.entrySet()) {
                        scoreList.add(new GamePresenter.ScorePair(entry.getKey(), entry.getValue()));
                    }
                    Collections.sort(scoreList);
                    scoreComponent.scores = scoreList;
                }
            }
        }, false);
        connection.connectToRoom(room);
    }
    @Override
    public void removedFromEngine(Engine engine) {

    }

    /*
        Update the local-player information on the game server.
     */
    @Override
    public void updateInterval() {
        for (Entity player : playerEntities) {
            PositionComponent pc = pm.get(player);
            VelocityComponent vc = vm.get(player);
            PlayerComponent plc = plm.get(player);

            MultiPlayerData mpd = makeMultiplayerData(player);
            //Send updated player data to server.
            connection.updatePlayerData(mpd);
        }
    }

    /*
        Helper function to make a multiplayerobject out of a playerentity. Multiplayerobjects are sent to the game server.
     */
    public MultiPlayerData makeMultiplayerData(Entity player) {
        PositionComponent pc = pm.get(player);
        VelocityComponent vc = vm.get(player);
        PlayerComponent plc = plm.get(player);

        MultiPlayerData mpd = new MultiPlayerData();
        mpd.name = this.name;
        mpd.x = pc.x;
        mpd.y = pc.y;
        mpd.vx = vc.x;
        mpd.vy = vc.y;
        mpd.score = plc.score;
        mpd.highscore = plc.highscore;
        mpd.connected = true;

        return mpd;
    }

}
