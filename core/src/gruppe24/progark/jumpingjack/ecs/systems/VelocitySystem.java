package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.VelocityComponent;

public class VelocitySystem extends IteratingSystem {
    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);

    float tst =0;

    public VelocitySystem(int priority) {
        super(Family.all(PositionComponent.class, VelocityComponent.class).get(), priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PositionComponent position = pm.get(entity);
        VelocityComponent velocity = vm.get(entity);

        /*
            Moves entities smoothly independent of framerate.
         */
        float dx = 0.5f * (computeVelocity(velocity.x, velocity.acceleration_x, velocity.drag_x, velocity.max_x, deltaTime) - velocity.x);
        velocity.x += dx;
        float delta = velocity.x * deltaTime;
        velocity.x += dx;
        position.x += delta;

        float dy = 0.5f * (computeVelocity(velocity.y, velocity.acceleration_y, velocity.drag_y, velocity.max_y, deltaTime) - velocity.y);
        velocity.y += dy;
        delta = velocity.y * deltaTime;
        velocity.y += dy;
        position.y += delta;
    }
    /*
        Utility function for computing smoother movement.
     */
    public float computeVelocity(float velocity, float acceleration, float drag, float max, float delta)
    {
        if (acceleration != 0)
        {
            velocity += acceleration * delta;
        }
        else if (drag != 0)
        {
            float dragDelta = drag * delta;
            if (velocity - dragDelta > 0)
            {
                velocity -= dragDelta;
            }
            else if (velocity + dragDelta < 0)
            {
                velocity += dragDelta;
            }
            else
            {
                velocity = 0;
            }
        }
        if ((velocity != 0) && (max != 0))
        {
            if (velocity > max)
            {
                velocity = max;
            }
            else if (velocity < -max)
            {
                velocity = -max;
            }
        }
        return velocity;
    }
}
