package gruppe24.progark.jumpingjack.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;

import java.util.ArrayList;

import gruppe24.progark.jumpingjack.EntityEventCallback;
import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.EntityBuilder;
import gruppe24.progark.jumpingjack.ecs.components.HitboxComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlatformComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.RenderComponent;
import gruppe24.progark.jumpingjack.ecs.components.VelocityComponent;
import gruppe24.progark.jumpingjack.model.PlayerData;

/*
    Handles player specific functionality, such as controls, player handling, etc.
 */
public class PlayerSystem extends IteratingSystem {

    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private ComponentMapper<PlayerComponent> plm = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<HitboxComponent> hbm = ComponentMapper.getFor(HitboxComponent.class);
    private ComponentMapper<RenderComponent> rm = ComponentMapper.getFor(RenderComponent.class);
    private ComponentMapper<PlatformComponent> platm = ComponentMapper.getFor(PlatformComponent.class);

    static int EVENT_PLAYER_DEATH = 666;

    public PlayerSystem(int priority) {
        super(Family.all(PositionComponent.class, VelocityComponent.class, PlayerComponent.class).get(), priority);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        // Listen for added player entities. When they are added subscribe to their collision events, unsubscribe when they are removed.
        Family family = Family.all(PositionComponent.class, VelocityComponent.class, PlayerComponent.class).get();
        engine.addEntityListener(family, new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                JumpingJack.instance.eventManager.subscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
            }
            @Override
            public void entityRemoved(Entity entity) {
                JumpingJack.instance.eventManager.unsubscribe(entity, CollisionSystem.EVENT_COLLISION, collisionCallback);
            }
        });
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PositionComponent position = pm.get(entity);
        VelocityComponent velocity = vm.get(entity);
        PlayerComponent player = plm.get(entity);
        HitboxComponent hitbox = hbm.get(entity);
        RenderComponent render = rm.get(entity);

        // Animate player character. Change image for when the player's velocity is over or less than 0.
        if (entity.getComponent(RenderComponent.class) != null) {
            if (velocity.y < 0) {
                render.img = JumpingJack.instance.assetManager.getImage(PlayerComponent.player_types_jump.get(PlayerData.getCurrentPlayer()));
            } else {
                render.img = JumpingJack.instance.assetManager.getImage(PlayerComponent.player_types.get(PlayerData.getCurrentPlayer()));
            }
        }


        //Check for touch events.
        boolean pDown = Gdx.input.isTouched();

        // Get position of pointer/cursor/touch.
        int px = Gdx.input.getX();

        //listener for horizontal movement
        if (pDown) {
            float speed = player.speed;
            float diffX = px - (float) Gdx.graphics.getWidth() / 2;
            if (diffX > 0) {
                velocity.acceleration_x = speed;
            } else {
                velocity.acceleration_x = -speed;
            }
        } else {
            velocity.acceleration_x = 0;
        }

        //to prevent the player from shooting over the top of the screen;
        //gameSpeed increases linearly with the players vertical speed, and the player is capped at 80% of the screen
        if (position.y >= (float) (JumpingJack.instance.gameHeight * .8) && velocity.y > 0) {
            position.y = (float) (JumpingJack.instance.gameHeight * .8);
            if (velocity.y > Math.abs(EntityBuilder.origGameSpeed)) {
                EntityBuilder.gameSpeed = -(int) velocity.y;
            } else {
                EntityBuilder.gameSpeed = EntityBuilder.origGameSpeed;
            }
        } else {
            EntityBuilder.gameSpeed = EntityBuilder.origGameSpeed;
        }

        // Handle player score.
        updateScore(entity);

        //player dies if out of bottom of screen
        if (position.y < 0 - hitbox.height) {
            player.deadState = true;
            //Calls eventsystem to nofify of player death. Used for telling multiplayersystem when to update highscore.
            JumpingJack.instance.eventManager.notify(EVENT_PLAYER_DEATH, entity, null);
        }

        // Check if player leaves the horisontal bounds of the gamescreen. Diffent behaviors depending on game difficulty.
        //hardmode off = player dies when out of bounds
        //hardmode on = horizontal edges now behave as walls, and the player may use them to bounce in the opposite direction
        if (LevelSystem.hardmode) {
            if (position.x < (0 - hitbox.width) || position.x > JumpingJack.instance.gameWidth) {
                player.deadState = true;
                //Calls eventsystem to nofify of player death. Used for telling multiplayersystem when to update highscore.
                JumpingJack.instance.eventManager.notify(EVENT_PLAYER_DEATH, entity, null);
            }
        } else if (position.x < 0) { // Player bounces off wall on easy.
            velocity.x = Math.abs(velocity.x);
        } else if (position.x > (JumpingJack.instance.gameWidth - hitbox.width)) {
            velocity.x = -Math.abs(velocity.x);
        }
    }

    /*
        Updates score.
     */
    private void updateScore(Entity entity) {
        PlayerComponent player = plm.get(entity);
        VelocityComponent vel = vm.get(entity);
        //score based on vertical distance travelled relative to the gamespeed
        player.velocityScore += (vel.y + (-EntityBuilder.gameSpeed)) / 100;
        //the "velocityScore" increases and decreases with the players vertical movement
        //the players current score should never decrease, so the final score is only updated when
        //"velocityscore" reached a new current max
        if (player.velocityScore > player.score) {
            player.score = (int)player.velocityScore;
        }
    }

    /*
        Callback for when a player collides with another entity.
     */
    private EntityEventCallback collisionCallback = new EntityEventCallback() {
        @Override
        public void entityEventCallback(Entity e1, Entity e2) {
            if (e2.getComponent(PlatformComponent.class) != null) {
                VelocityComponent v = vm.get(e1);
                PositionComponent pp = pm.get(e2);
                PositionComponent ps = pm.get(e1);
                HitboxComponent hbox = hbm.get(e2);
                PlatformComponent plat = platm.get(e2);
                if (v.y < 0 && ps.y > (pp.y + (float)hbox.height / 2)) {
                    PlayerComponent p = plm.get(e1);
                    if (plat.type == PlatformComponent.TYPE_YELLOW) {
                        v.y = p.jumpSpeed * 2;
                    } else {
                        v.y = p.jumpSpeed;
                    }
                }
            }
        }
    };
}
