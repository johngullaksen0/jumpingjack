package gruppe24.progark.jumpingjack.ecs;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;

import java.lang.ref.PhantomReference;

import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.ecs.components.CoinComponent;
import gruppe24.progark.jumpingjack.ecs.components.HitboxComponent;
import gruppe24.progark.jumpingjack.ecs.components.MultiplayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlatformComponent;
import gruppe24.progark.jumpingjack.ecs.components.PlayerComponent;
import gruppe24.progark.jumpingjack.ecs.components.PositionComponent;
import gruppe24.progark.jumpingjack.ecs.components.PowerupComponent;
import gruppe24.progark.jumpingjack.ecs.components.RenderComponent;
import gruppe24.progark.jumpingjack.ecs.components.VelocityComponent;
import gruppe24.progark.jumpingjack.ecs.systems.PlatformSystem;
import gruppe24.progark.jumpingjack.ecs.systems.PowerupSystem;

public class EntityBuilder {

    public static final int origGameSpeed = -200;
    public static int gameSpeed = -200;

    /*
    Creates pre-built player-entity.
     */
    public static Entity makePlayer(float x, float y,int type) {
        Entity entity = new Entity();
        RenderComponent render = new RenderComponent();
        //render.img = JumpingJack.instance.assetManager.getImage("blueplayerstand");

        switch(type) {
            case PlayerComponent.PLAYERTYPE_BLUE:
                render.img = JumpingJack.instance.assetManager.getImage("blueplayerstand");
                break;
            case PlayerComponent.PLAYERTYPE_RED:
                render.img = JumpingJack.instance.assetManager.getImage("redplayerstand");
                break;
            case PlayerComponent.PLAYERTYPE_INGVILD:
                render.img = JumpingJack.instance.assetManager.getImage("ingvildstand");
                break;
            case PlayerComponent.PLAYERTYPE_AGNES:
                render.img = JumpingJack.instance.assetManager.getImage("agnesstand");
                break;
            case PlayerComponent.PLAYERTYPE_CATHRINE:
                render.img = JumpingJack.instance.assetManager.getImage("cathrinestand");
                break;

            default:
        }

        entity.
                add(new PositionComponent(x, y)).
                add(new VelocityComponent(0, 0, 0, -700, 1000, 5000, 50, 0)).
                add(render).
                add(new HitboxComponent(render.img.getWidth(), render.img.getHeight())).
                add(new PlayerComponent(type));

        return entity;
    }

    /*
    Creates pre-built multiplayer-entity.
     */
    public static Entity makeMultiPlayer(float x, float y, String name) {
        Entity entity = new Entity();
        RenderComponent render = new RenderComponent();
        render.img = JumpingJack.instance.assetManager.getImage("player2");
        entity.
                add(new PositionComponent(x, y)).
                add(new VelocityComponent(0, 0, 0, 0, 1000, 10000, 50, 0)).
                //add(render).  //Commented out, because movement of online players is janky. Needs to be improved before it could be used.
                add(new MultiplayerComponent(name));

        return entity;
    }

    /*
        Creates pre-built Platform-entity.
     */
    public static Entity makePlatform(float x, float y, int type) {
        Entity entity = new Entity();
        RenderComponent render = new RenderComponent();

        switch(type) {
            case PlatformComponent.TYPE_GREEN:
                render.img = JumpingJack.instance.assetManager.getImage("greenplatform");
                break;
            case PlatformComponent.TYPE_RED:
                render.img = JumpingJack.instance.assetManager.getImage("redplatform");
                break;
            case PlatformComponent.TYPE_YELLOW:
                render.img = JumpingJack.instance.assetManager.getImage("yellowplatform");
                break;
            case PlatformComponent.TYPE_BLUE:
                render.img = JumpingJack.instance.assetManager.getImage("blueplatform");
                break;
            case PlatformComponent.TYPE_BLACK:
                render.img = JumpingJack.instance.assetManager.getImage("blackplatform");
                break;
            default:
                // code block
        }

        entity.
                add(new PositionComponent(x, y)).
                add(new VelocityComponent(0, gameSpeed, 0, 0, 1000, 500, 50, 0)).
                add(render).
                add(new HitboxComponent(render.img.getWidth(), render.img.getHeight())).
                add(new PlatformComponent(type));

        return entity;
    }
    public static Entity makePlatform(float x, float y) {
        return makePlatform(x, y, PlatformComponent.TYPE_GREEN);
    }
    public static Entity makePowerup(float x, float y, int type) {
        Entity entity = new Entity();
        RenderComponent render = new RenderComponent();
        String img = PowerupSystem.powerup_types.get(type).get(0);
        render.img = JumpingJack.instance.assetManager.getImage(img);
        entity.
                add(new PositionComponent(x, y)).
                add(new VelocityComponent(0, gameSpeed, 0, 0, 0, 0, 0, 0)).
                add(render).
                add(new HitboxComponent(render.img.getWidth(), render.img.getHeight())).
                add(new PowerupComponent(type));
        return entity;
    }
    public static Entity makeCoin(float x, float y, int value) {
        Entity entity = new Entity();
        RenderComponent render = new RenderComponent();
        render.img = JumpingJack.instance.assetManager.getImage("coin1");
        entity.
                add(new PositionComponent(x, y)).
                add(new VelocityComponent(0, gameSpeed, 0, 0, 0, 0, 0, 0)).
                add(render).
                add(new HitboxComponent(render.img.getWidth(), render.img.getHeight())).
                add(new CoinComponent(value));
        return entity;
    }
    public static Entity makeCoin(float x, float y) {
        return makeCoin(x, y, 1000);
    }

}

