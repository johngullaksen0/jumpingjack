package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

/*
    Position component, includes data for an entity's position.
 */
public class PositionComponent implements Component {
    public float x;
    public float y;

    public PositionComponent(float x, float y) {
        this.x = x;
        this.y = y;
    }
    public PositionComponent() {
        this(0, 0);
    }
}
