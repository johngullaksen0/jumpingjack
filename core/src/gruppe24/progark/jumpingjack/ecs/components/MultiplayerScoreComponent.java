package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

import java.util.ArrayList;
import java.util.HashMap;

import gruppe24.progark.jumpingjack.ui.presenter.GamePresenter;

public class MultiplayerScoreComponent implements Component {

    public ArrayList<GamePresenter.ScorePair> scores = new ArrayList<>();
}
