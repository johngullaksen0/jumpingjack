package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

/*
    Component for the hitbox. Used for collisions and physics calculation.
 */
public class HitboxComponent implements Component {

    public int width;
    public int height;

    public HitboxComponent() {
        this(0, 0);
    }
    public HitboxComponent(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
