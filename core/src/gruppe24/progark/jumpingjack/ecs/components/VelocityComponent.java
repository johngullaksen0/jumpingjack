package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

/*
    A component used for calculating the movement of an entity, such as velocity, acceleration and drag.
 */
public class VelocityComponent implements Component {

    // Velocity is measured in pixels per second.
    public float x;
    public float y;
    // Max speed.
    public float max_x;
    public float max_y;
    public float drag_x;
    public float drag_y;
    public float acceleration_x;
    public float acceleration_y;

    public VelocityComponent() {
        this(0, 0, 0, 0, 0, 0, 0, 0);
    }
    public VelocityComponent(float x, float y, float acceleration_x, float acceleration_y, float max_x, float max_y, float drag_x, float drag_y) {
        this.x = x;
        this.y = y;
        this.acceleration_x = acceleration_x;
        this.acceleration_y = acceleration_y;
        this.max_x = max_x;
        this.max_y = max_y;
        this.drag_x = drag_x;
        this.drag_y = drag_y;
    }
}
