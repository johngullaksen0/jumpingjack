package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

import java.util.HashMap;
import java.util.Map;

/*
    Component for Platform. Includes type of platform.
 */
public class PlatformComponent implements Component {
    public static final int TYPE_GREEN = 1;
    public static final int TYPE_RED = 2;
    public static final int TYPE_YELLOW = 3;
    public static final int TYPE_BLUE = 4;
    public static final int TYPE_BLACK = 5;

    public int type;

    public PlatformComponent() {
        this(TYPE_GREEN);
    }

    public PlatformComponent(int type) {
        this.type = type;
    }
}
