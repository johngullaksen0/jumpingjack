package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

import gruppe24.progark.jumpingjack.ecs.systems.PowerupSystem;

public class PowerupComponent implements Component {
    public float jumpMultiplier;
    public float duration;
    public boolean isActive;
    public float time;

    public int type;

    public PowerupComponent(int type) {
        this.type = type;
        this.jumpMultiplier = Float.parseFloat(PowerupSystem.powerup_types.get(type).get(1));
        this.duration = Integer.parseInt(PowerupSystem.powerup_types.get(type).get(2));
    }
}
