package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

import java.util.HashMap;
import java.util.Map;

/*
    Component for Player. Includes playerspecific data such as their movement speed, etc.
 */
public class PlayerComponent implements Component {



    public float jumpSpeed = 500;
    public final float origJumpSpeed = 500;
    public float speed = 600;
    public int score = 0;
    public int highscore = 0;
    public float velocityScore = 0;
    public boolean deadState = false;


    //Defines different types of game characters
    public static final int PLAYERTYPE_BLUE = 1;
    public static final int PLAYERTYPE_RED = 2;
    public static final int PLAYERTYPE_INGVILD = 3;
    public static final int PLAYERTYPE_AGNES = 4;
    public static final int PLAYERTYPE_CATHRINE = 5;

    public static final Map<Integer, String> player_types = new HashMap<Integer, String>() {{
        put(1, "blueplayerstand");
        put(2, "redplayerstand");
        put(3,"ingvildstand");
        put(4,"agnesstand");
        put(5,"cathrinestand");
    }};
    public static final Map<Integer, String> player_types_jump = new HashMap<Integer, String>() {{
        put(1, "blueplayerjump");
        put(2, "redplayerjump");
        put(3,"ingvildjump");
        put(4,"agnesjump");
        put(5,"cathrinejump");
    }};

    public int type;

    public PlayerComponent(int type) {
        this.type = type;
    }






}
