package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

public class MultiplayerComponent implements Component {

    public int score = 0;
    public int highscore = 0;
    public String name = "noName";

    public MultiplayerComponent(String name) {
        this.name = name;
    }
    public MultiplayerComponent() {
        this("noName");
    }

}
