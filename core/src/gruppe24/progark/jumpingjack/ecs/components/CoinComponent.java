package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;

public class CoinComponent implements Component {


        public int value;

        public CoinComponent() {
            this(1000);
        }

        public CoinComponent(int value) {
            this.value = value;
        }
}
