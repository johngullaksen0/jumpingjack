package gruppe24.progark.jumpingjack.ecs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Texture;

import java.util.HashMap;
import java.util.Map;

/*
    A component used for drawing entities on screen. Has data neccessary for it's task, such as an image, etc.
 */
public class RenderComponent implements Component {
    public Texture img;

}
