package gruppe24.progark.jumpingjack;

import com.badlogic.ashley.core.Entity;

/*
    Callback interface meant to be used with EventManager. 'entityEventCallback' is called when an event occurs.
 */
public interface EntityEventCallback {

    public void entityEventCallback(Entity e1, Entity e2);
}
