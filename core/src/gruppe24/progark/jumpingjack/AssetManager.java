package gruppe24.progark.jumpingjack;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

import java.util.HashMap;

/*
    Keeps track of any assets such as images, sounds, and etc, needed in the game.
 */
public class AssetManager {

    private HashMap<String, Texture> images;
    private HashMap<String, Sound> sounds;

    public AssetManager() {
        images = new HashMap<>();
        sounds = new HashMap<>();
    }
    public void loadImages() {
        //Platforms
        loadImage("greenplatform", "greenplatform.png");
        loadImage("redplatform", "redplatform.png");
        loadImage("yellowplatform", "yellowplatform.png");
        loadImage("blueplatform", "blueplatform.png");
        loadImage("blackplatform", "blackplatform.png");
        //powerups
        loadImage("spring", "spring.png");
        loadImage("energydrink", "energydrink.png");
        loadImage("portal", "portal.png");
        loadImage("jetpack", "jetpack.png");
        //jetpack animation
        loadImage("jetpack1", "jetpack1.png");
        loadImage("jetpack2", "jetpack2.png");
        //coins for animation
        loadImage("coin1", "coin1.png");
        loadImage("coin2", "coin2.png");
        loadImage("coin3", "coin3.png");
        //Players
        loadImage("redplayerstand","redplayerstand.png");
        loadImage("redplayerjump","redplayerjump.png");
        loadImage("blueplayerstand","blueplayerstand.png");
        loadImage("blueplayerjump","blueplayerjump.png");
        loadImage("ingvildstand","ingvildstand.png");
        loadImage("ingvildjump","ingvildjump.png");

        loadImage("agnesstand","agnesstand.png");
        loadImage("agnesjump","agnesjump.png");
        loadImage("cathrinestand","cathrinestand.png");
        loadImage("cathrinejump","cathrinejump.png");

        //tutorials
        loadImage("tutorial1", "tutorial1.png");
        loadImage("tutorial2", "tutorial2.png");
        loadImage("tutorial3", "tutorial3.png");
        //backgrounds
        loadImage("gym", "gym.png");
        loadImage("gymbackground", "gymwindows.png");
        loadImage("oceangym", "oceangym.png");
        loadImage("citygym", "citygym.png");
        loadImage("summerbackground", "summerbackground.png");
        loadImage("winterbackground", "winterbackground.png");
        loadImage("summer", "summer.png");
        loadImage("winter", "winter.png");


    }
    public void loadSounds() {

    }
    /*
        Loads the file defined in the 'path' parameter and saves it as 'name'.
     */
    public String loadImage(String name, String path) {
        images.put(name, new Texture(path));
        return name;
    }
    /*
        Loads the file defined in the 'path' parameter and saves it as 'name'.
     */
    public String loadSound(String name, String path) {
        sounds.put(name, Gdx.audio.newSound(new FileHandle(path)));
        return name;
    }

    /*
        Returns the image saved in 'name'.
     */
    public Texture getImage(String name) {
        return images.get(name);
    }
    /*
        Returns the sound saved in 'name'.
     */
    public Sound getSound(String name) {
        return sounds.get(name);
    }
}
