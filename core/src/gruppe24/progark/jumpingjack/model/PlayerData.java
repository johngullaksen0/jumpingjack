package gruppe24.progark.jumpingjack.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

import java.util.HashMap;
import java.util.Map;

import gruppe24.progark.jumpingjack.MyGdxGame;

public class PlayerData {
    private static Music backgroundMusic;
    private static int currentPlayer = 1;
    private static int currentBackground = 1;
    private static Texture background;


    //Defines different types of game backgrounds
    public static final int GYM = 1;
    public static final int SUMMER = 2;
    public static final int WINTER = 3;

    public static final Map<Integer, String> backgrounds = new HashMap<Integer, String>() {{
        put(1, "gym");
        put(2, "summer");
        put(3,"winter");

    }};

    public PlayerData(){
       backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("JumpingJackSong.mp3"));
       if(!backgroundMusic.isPlaying() && MyPreferences.isMusicEnabled()){
           turnOnMusic();
       }
       background = new Texture(backgrounds.get(currentBackground) + ".png");
    }


    public static void turnOnMusic(){
       backgroundMusic.setLooping(true);
       backgroundMusic.play();
       backgroundMusic.isPlaying();
    }

    public static void turnOfMusic(){
       backgroundMusic.stop();
   }

    public static int getCurrentPlayer() {
        return currentPlayer;
    }

    public static void setCurrentPlayer(int currentPlayer) {
        PlayerData.currentPlayer = currentPlayer;
    }

    public static int getCurrentBackground() {
        return currentBackground;
    }

    public static void setCurrentBackground(int currentBackground, Texture texture) {
        PlayerData.currentBackground = currentBackground;
        background = texture;
    }

    public static Texture getBackground() {
        return background;
    }
}
