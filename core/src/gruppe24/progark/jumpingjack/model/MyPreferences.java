package gruppe24.progark.jumpingjack.model;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class MyPreferences {

    private static int highScore;
    public static Preferences preferences = Gdx.app.getPreferences("My Preferences");


    public MyPreferences() {
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public static int getHighScore(String mode) {
        mode += "HighScore";
        return preferences.getInteger(mode, 0);
    }


    public static void setHighScore(String mode, int score) {
        mode += "HighScore";
        preferences.putInteger(mode, score);
        preferences.flush();
    }

    public static boolean isMusicEnabled() {
        return preferences.getBoolean("MusicOn", true);
    }

    public static void setMusicEnabled(boolean enabled) {
        preferences.putBoolean("MusicOn", enabled);
        preferences.flush();
    }

    public static boolean isHardMode(){
        return preferences.getBoolean("HardModeOn", false);
    }

    public static void setHardMode(boolean enabled){
        preferences.putBoolean("HardModeOn", enabled);
        preferences.flush();
    }
    public static void setFirstTimePlaying(boolean enabled){
        preferences.putBoolean("FirstTimePlaying", enabled);
        preferences.flush();
    }
    public static boolean isFirstTimePlaying(){
        return preferences.getBoolean("FirstTimePlaying", true);
    }

}
