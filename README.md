# JumpingJack

A jumping game made for Android. Jump on platforms and survive for as long as possible. Uses Libgdx and the Ashley ECS framework. Also uses firebase for some barebones rudimentary multiplayer and online scorekeeping.

[Libgdx](https://www.libgdx.com/)

[Ashley ECS](https://www.github.com/libgdx/ashley)

[Firebase](https://www.firebase.google.com/)

