package gruppe24.progark.jumpingjack.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import gruppe24.progark.jumpingjack.JumpingJack;
import gruppe24.progark.jumpingjack.MyGdxGame;
import gruppe24.progark.jumpingjack.ecs.systems.MultiplayerSystem;
import gruppe24.progark.jumpingjack.util.network.ServerConnection;

public class DesktopLauncher {
	public static void main (String[] arg) {
		ServerConnection connection = new ServerConnection();
		MultiplayerSystem.connection = connection;

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.foregroundFPS = 60;
		config.width = JumpingJack.instance.gameWidth;
		config.height = JumpingJack.instance.gameHeight;
		new LwjglApplication(new MyGdxGame(), config);
	}
}
